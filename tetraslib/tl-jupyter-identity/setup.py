from setuptools import find_packages
from setuptools import setup

setup(
    name="tl-jupyter-identity",
    version='0.1',
    description="A library for identify and authenticate users in JupyterLab",
    author="Tetras-Libre",
    license='AGPL',
    packages=find_packages(),
    install_requires=["jupyterlab", "requests"],
    zip_safe=False,
)
