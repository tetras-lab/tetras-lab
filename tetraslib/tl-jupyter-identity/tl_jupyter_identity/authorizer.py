from typing import Any
from jupyter_server.base.handlers import JupyterHandler
from jupyter_server.auth import Authorizer
from pprint import pprint


class TlJupyterAuthorizer(Authorizer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.log.debug(f"\n\n-----------------> TlJupyterAuthorizer initialized\n")

    def is_authorized(self, handler: JupyterHandler, user: Any, action: str, resource: str):
        if not user or user.username == 'anonymous':
            self.log.debug(f"\n\n-----------------> user anonymous or no user\n")
            return False
        if action == 'write' and resource == 'contents' and self.is_user_in_wrong_folder(handler, user):
            admin = getattr(user, 'admin')
            if admin and admin == 'True':
                self.log.debug(f"\n\n-----------------> user admin, grant all privileges\n")
                return True
            self.log.debug(f"\n\n-----------------> action denied: {action}\n")
            return False
        self.log.debug(f"\n\n-----------------> action: {action}\n")
        self.log.debug(f"\n\n-----------------> resource: {resource}\n")
        self.log.debug(f"\n\n-----------------> handler: {pprint(vars(handler))}\n")
        self.log.debug(f"\n\n-----------------> self: {pprint(vars(self))}\n")
        return True

    def is_user_in_wrong_folder(self, handler, user):
        path_kwargs = getattr(handler, 'path_kwargs')
        self.log.debug(f"\n\n-----------------> path_kwargs: {path_kwargs}\n")
        path_args = getattr(handler, 'path_args')
        self.log.debug(f"\n\n-----------------> path_args: {path_args}\n")
        path_kwargs_value = path_kwargs.get('path', '')
        self.log.debug(f"\n\n-----------------> path_kwargs_value: {path_kwargs_value}\n")
        if len(path_args):
            path_args_value = path_args[0]
        else:
            path_args_value = None
        self.log.debug(f"\n\n-----------------> path_args_value: {path_args_value}\n")
        if path_args_value:
            return self.not_correct_path(path_args_value, user)
        if path_kwargs_value:
            return self.not_correct_path(path_kwargs_value[1:], user)
        return False

    @staticmethod
    def not_correct_path(path, user):
        return not path.startswith('dashboards/' + user.username) and not path.startswith('dashboards/shared')
