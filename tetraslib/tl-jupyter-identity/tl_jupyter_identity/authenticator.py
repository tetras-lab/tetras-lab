from jupyter_server.auth import IdentityProvider, User
from urllib.parse import urlparse, parse_qs
from pathlib import Path
from pprint import pprint


class TlJupyterAuthenticator(IdentityProvider):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.known_user = User('anonymous', 'Anonymous', 'Anonymous', 'AA', None, None)
        self.log.debug(f"\n-----------------> TlJupyterAuthenticator initialized")
        self.log.debug(f"\n-----------------> user {self.known_user.username}\n")

    def get_user(self, handler):
        if handler.request:
            self.log.debug(f"\n\n-----------------> handler: {pprint(vars(handler))}\n")
            parsed_url = urlparse(handler.request.uri)
            parsed_query_string = parse_qs(parsed_url.query)

            if 'user' not in parsed_query_string:
                if self.known_user.username != 'anonymous':
                    # user is already known
                    self.log.debug(f"\n-----------------> no relevant handler request {handler.request.uri}\n")
                    self.log.debug(f"\n-----------------> return '{self.known_user.username}'\n")
                    self.set_cookie(handler, self.known_user)
                    # the following line will load the cookie
                    return super().get_user(handler)
                else:
                    # user is unknown
                    self.log.debug(f"\n-----------------> no user connected\n")
                    # the following line may be too permissive
                    return super().get_user(handler)

            if 'user' in parsed_query_string:
                self.log.debug(f"\n-----------------> relevant handler request {handler.request.uri}\n")
                if self.known_user.username != 'anonymous':
                    self.log.debug(f"\n-----------------> user is already known\n")
                    # check if query_string_user is the same as known_user
                    user = self.get_user_from_query_string(parsed_query_string)
                    if self.is_know_user(user):
                        self.log.debug(f"\n-----------------> return: {self.known_user.username}\n")
                        self.create_folders(user.username)
                        return self.known_user
                    else:
                        self.log.debug(f"\n-----------------> user {self.known_user.username} will be replaced by {user.username}!\n")
                else:
                    # new user
                    self.log.debug(f"\n-----------------> new user!\n")
                    user = self.get_user_from_query_string(parsed_query_string)
                # store user in both client cookie and authenticator object
                self.set_cookie(handler, user)
                self.known_user = user
                # create relevant folders if needed
                self.create_folders(user.username)
                self.log.debug(f"\n-----------------> create folders for {user.username}\n")
                self.log.debug(f"\n-----------------> return: {self.known_user.username}\n")
                # the following line will load the cookie
                return super().get_user(handler)
        return super().get_user(handler)

    @staticmethod
    def get_user_from_query_string(parsed_query_string):
        user_query_string = parse_qs(parsed_query_string['user'][0])
        if 'username' not in user_query_string:
            raise Exception("username not found")
        username = user_query_string['username'][0]
        name = user_query_string['name'][0]
        display_name = user_query_string['display_name'][0]
        initials = user_query_string['initials'][0]
        avatar_url = None if user_query_string['avatar_url'][0] == 'None' else user_query_string['avatar_url'][0]
        color = None if user_query_string['color'][0] == 'None' else user_query_string['color'][0]
        admin = None if user_query_string['admin'][0] == 'None' else user_query_string['admin'][0]
        user = User(username, name, display_name, initials, avatar_url, color)
        setattr(user, 'admin', admin)
        return user

    def create_folders(self, username):
        self.log.debug(f"\n-----------------> dir: {Path('/opt/dashboards/shared').absolute()}\n")
        Path("/opt/dashboards/shared").mkdir(parents=True, exist_ok=True)
        Path(f"/opt/dashboards/{username}").mkdir(parents=True, exist_ok=True)

    def set_cookie(self, handler, user):
        self.log.debug(f"\n-----------------> Setting cookie for {user.username}")
        cookie_name = self.get_cookie_name(handler)
        cookie = handler.get_cookie(cookie_name)
        if cookie is not None:
            self.log.debug(f"\n-----------------> Clearing invalid/expired login cookie {cookie_name}")
            self.clear_login_cookie(handler)
        self.set_login_cookie(handler, user)

    def is_know_user(self, user):
        return self.known_user.username == user.username \
               and self.known_user.name == user.name \
               and self.known_user.display_name == user.display_name \
               and self.known_user.initials == user.initials \
               and self.known_user.avatar_url == user.avatar_url \
               and self.known_user.color == user.color
