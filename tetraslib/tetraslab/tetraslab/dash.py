import socket, errno, os
from dash import Dash as RealDash


class Dash(RealDash):

    def __init__(self, *args, **kwargs):
        self.server_url, self.host, self.port, self.proxy, self.base_path = Dash._get_tl_config()
        kwargs['requests_pathname_prefix'] = self.base_path
        super().__init__(*args, **kwargs)

    def run(self, *args, **kwargs):
        kwargs['host'] = self.host
        kwargs['port'] = self.port
        kwargs['proxy'] = self.proxy
        kwargs['jupyter_server_url'] = self.server_url
        os.environ['HOST'] = self.host
        os.environ['PORT'] = f'{self.port}'
        os.environ['DASH_PROXY'] = self.proxy
        super().run(*args, **kwargs)

    @staticmethod
    def _get_tl_config():
        # Find a free port
        host = "0.0.0.0"
        port = 8050
        end = 9999
        found = False
        while not found:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                try:
                    s.bind((host, port))
                    found = True
                except socket.error as e:
                    if e.errno == errno.EADDRINUSE:
                        port = port + 1
                        if port > end:
                            raise "No available APP port"
                    else:
                        raise e
        if os.getenv("HOST", None) is not None:
            proto = os.getenv("PROTO")
            actualhost = os.getenv("JUPYTER_HOST", os.getenv("VOILA_HOST", ""))
            localport = os.getenv("PORT", 80)
            intermediatehost = os.getenv("HOST", "localhost")
            base_path = f"/{actualhost}/app_proxy/{port}/"
            proxified= f"{proto}://{intermediatehost}:{localport}"
            localurl = f"http://{host}:{port}"
            proxy = f"{localurl}::{proxified}"
            return proxified, host, port, proxy, base_path
        return f"http://localhost:{port}", host, port, None, "/"
