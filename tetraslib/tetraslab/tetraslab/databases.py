def get_engine(backend):
    from sqlalchemy import create_engine
    from os import environ
    import json

    if backend == "mariadb":
        env_key = "mariadb_datascience"
        token = "mariadb+pymysql"
    elif backend == "timescaledb":
        env_key = "timescaledb"
        token = "postgresql"
    elif backend == "monetdb":
        env_key = "monetdb"
        token = "monetdb"
    else:
        raise NotImplementedError

    env = json.loads(environ["LOCAL_DB_CREDENTIALS"])[env_key]
    return create_engine(
        token + "://{}:{}@{}:{}/{}".format(env["user"], env["password"], env["host"], env["port"], env["name"]))
