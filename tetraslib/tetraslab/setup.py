from setuptools import find_packages
from setuptools import setup

setup(name='tetraslab',
      version='0.1',
      description='A library for dealing with TetrasLab environment.',
      author='Tetras-Libre',
      license='AGPL',
      packages=find_packages(),
      zip_safe=False,
      install_requires=[
          'sqlalchemy',
          'dash'
      ])
