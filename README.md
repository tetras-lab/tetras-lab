## TOC

[[_TOC_]]

## Tetras BI

This is a docker stack using django + voila stack to serve notebooks via voila to authenticated users.

## Presentation

This stack is intended deployed several times on the same server with docker isolation for different groups of user.

Each group of user uses the same instance that have access to a private DB for data that are only avaiable to the groupe while all users can access to data shared in the shared database.

The private DB can be virtually any database that has a docker image, currently we provide configurations for [monetdb](https://www.monetdb.org/), [fuseki](https://jena.apache.org/documentation/fuseki2/) and [timescaledb](https://docs.timescale.com/) with [PostGIS](https://postgis.net/) enabled.

Each group of users have access to a private set of scripts to import data and dashboards (notebooks).

The notebooks are readonly.

### Docker stack

```mermaid
graph LR

  /opt --> /opt/public-files

  django -->|http://../dashboard/id| voila
  django -->|http://../jupyter| jupyter(jupyter lab)
  django -->|http://../dashboards/id| nbviewer
  django --> mariadb[(mariadb for django)]
  voila --> DB1[(Some database)]
  jupyter --> DB1
  voila --> DB2[(Another database)]
  jupyter --> DB2

  jupyter --> /opt
  voila --> /opt
```

## Install

```bash
# Retrieve the sources
git clone https://gitlab.com/tetras-lab/tetras-lab.git
cd tetras-lab

# Retrive your dashboard, see [requirements](#requirements)
git clone <dashboard repository> opt

cp .env.sample .env
```

Open .env file with your favorite editor 

Read this file carefully and set the variables to fit your needs

The following variables **MUST** be configured **BEFORE** you launch the application for the first time
+ `COMPOSE_FILE` (see the comment above the variable)
+ `DB_PASSWORD` (random string)
+ `JUPYTER_TOKEN` (random string)

```
# Change owner of TetrasBI/data to give access to the Jupyter docker
# Do chown only if system is not mac
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac
[ ! "${machine}" = "Mac" ] && chown -R 1000:1000 TetrasBI/data

# Build and run the application
docker-compose up --build
```

Wait for mariadb initialization. It will look like:

```
# mariadb_1   | 2022-07-26 10:07:22+00:00 [Note] [Entrypoint]: MariaDB init process done. Ready for start up.                                                                                                         mariadb_1   |                                                                                                                                                                                                       
# ....
# mariadb_1   | 2022-07-26 10:07:23 0 [Note] InnoDB: Buffer pool(s) load completed at 220726 10:07:23                                                                                                                 mariadb_1   | 2022-07-26 10:07:23 0 [Note] mariadbd: ready for connections.
```

Restart the application (so django can run it's migrations)

```
Ctrl-C
docker-compose down && docker-compose up
```

### Databases

#### Monetdb

+ Stop the stack
+ Add `:monetdb.yml` to the `COMPOSE_FILE` variable in your `.env`
+ Set the db password
```sh
docker-compose up -d
# Set your database password
docker-compose exec monetdb /home/monetdb/set-monetdb-password.sh <Password>
```
+ Then adapt the `LOCAL_DB_CREDENTIALS` in you .env accordingly

#### Fuseki

+ Stop the stack
+ Add to the `COMPOSE_FILE` variable in your `.env`, either:
    + `:fuseki.yml:fuseki-dev.yml`  to expose fuseki on http://localhost:3030 (the port can be configured in fuseki-dev.yml), or
    + `:fuseki.yml:fuseki-prod.yml`  to expose fuseki on the `FUSEKI_HOST` URL defined in `.env`
+ Set the `FUSEKI_*` variables in the `.env`
+ Restart the stack

#### TimescaleDB

+ Stop the stack
+ Add `:timescaledb.yml` to the `COMPOSE_FILE` variable in your `.env`
+ Set the `POSTGRES_*` variables in the `.env`
+ Restart the stack

## Run

```
docker-compose up
```

And go to `http://localhost:8000`

## Customize 

See [the wiki](https://gitlab.com/tetras-lab/tetras-lab/-/wikis/admin_manual#customizing-t%C3%A9tras-lab)

## Production consideration

The `prod.yml` suppose that you have a traefik entrypoint configured and running in an external `traefik` docker network with a resolver nammed `myresolver`, fell free to copy it and suit it for your needs.

It also presumes that traefik already defines a `hardening` middleware like :

```
- "traefik.http.middlewares.hardening.headers.sslredirect=true"
- "traefik.http.middlewares.hardening.headers.forceSTSHeader=true"
- "traefik.http.middlewares.hardening.headers.stsIncludeSubdomains=true"
- "traefik.http.middlewares.hardening.headers.stsSeconds=15552000"
- "traefik.http.middlewares.hardening.headers.stsPreload=true"
- "traefik.http.middlewares.hardening.headers.referrerPolicy=no-referrer"
- "traefik.http.middlewares.hardening.headers.customFrameOptionsValue=SAMEORIGIN"
```


### Recommended packages to deploy on a server

We like to deploy Tetras-BI on Debian servers vith the following packages installed :

*  `git` (basically required...)
*  `git-lfs` (optional)

## Wiki

Please take a look of [our wiki](https://gitlab.com/tetras-lab/tetras-lab/-/wikis/home)
