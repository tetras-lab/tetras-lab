# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import ChoiceField, MultipleChoiceField, ModelForm, BooleanField, Select
from TetrasBI.widgets import TetrasBIToggleSwitchWidget, TetrasBIFileInputWidget, TetrasBIHiddenTextWithBooleanWidget, TetrasBISelectMultipleWidget
from TetrasBI.forms.tlform import TlModelForm
from .models.dashboard import Dashboard
from django.utils.translation import gettext_lazy as _


class DashboardEditForm(ModelForm, TlModelForm):
    path = ChoiceField(choices=[], widget=Select, label=_('Path'))

    def get_potential_dashboards(self, exclude=[]):
        return enumerate(Dashboard.objects.getPotentialDashboards(allow_existing=True, exclude=exclude))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # This is a hack : choices are dynamic so we do not initalize them with de class attribute
        # but we set them a init time
        # See issue #227
        self.fields['path'].choices = [('', '--')] + [(v, v) for (v, v) in self.get_potential_dashboards(exclude=['tl_hidden.ipynb', 'tl_dashboards_hidden.ipynb'])]
        self.fields['path'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Dashboard
        fields = ['path', 'title', 'sourcesVisible', 'precomputed', 'description', '_img']
        labels = {
            'title': _('Title'),
            'sourcesVisible': _('Sources Visible'),
            'precomputed': _('Precomputed'),
            'description': _('Description'),
        }
        icons = {
            'path': 'file-search-outline',
            'title': 'format-title',
            'precomputed': 'cog',
            'description': 'card-text-outline',
            'sourcesVisible': 'file-code-outline',
        }
        on_icon = {
            'sourcesVisible': "<i class='mdi mdi-file-code-outline mr-1'></i>",
            'precomputed': "<i class='mdi mdi-notebook-check-outline mr-1'></i>",
        }
        off_icon = {
            'sourcesVisible': "<i class='mdi mdi-file-code-outline mr-1'></i>",
            'precomputed': "<i class='mdi mdi-notebook-outline mr-1'></i>",
        }
        on_text = {
            'sourcesVisible': _("Togglable"),
            'precomputed': _("NbViewer"),
        }
        off_text = {
            'sourcesVisible': _("Hidden"),
            'precomputed': _("Voilà"),
        }
        on_style = {
            'sourcesVisible': 'success',
            'precomputed': 'secondary',
        }
        off_style = {
            'sourcesVisible': 'secondary',
            'precomputed': 'success',
        }
        widgets = {}
        for key in ['sourcesVisible', 'precomputed']:
            widgets[key] = TetrasBIToggleSwitchWidget(
                on_icon=on_icon[key],
                off_icon=off_icon[key],
                on_text=on_text[key],
                off_text=off_text[key],
                on_style=on_style[key],
                off_style=off_style[key],
            )
        widgets['_img'] = TetrasBIFileInputWidget(
            delete_label=_("Delete image"),
            delete_icon='image-off-outline',
            delete_on_icon="<i class='mdi mdi-check mr-1'></i>",
            delete_off_icon="<i class='mdi mdi-close mr-1'></i>",
            delete_on_text=_("Yes"),
            delete_off_text=_("No"),
            delete_on_style='danger',
            delete_off_style='secondary',
            input_label=_('Image'),
            input_icon='image-outline',
            input_accept='image/*',
            input_browse_text=_("Choose file"),
        )


class DashboardShareForm(ModelForm, TlModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        dash = kwargs.pop('dash')
        base_url = kwargs.pop('base_url')
        token = kwargs.pop('token')
        disabled = kwargs.pop('disabled')
        options = kwargs.pop('options')
        super().__init__(*args, **kwargs)
        # Every user who has at least a minimal right
        if user.has_perm('dashboards.share_dashboard', dash) or user.has_perm('dashboards.share_dashboard_by_link', dash):
            self.fields['public_link'] = BooleanField(
                required=False,
                widget=TetrasBIHiddenTextWithBooleanWidget(
                    label=_("Public link"),
                    icon="earth",
                    base_url=base_url,
                    token=token,
                    copy_label=_("Copy"),
                    copy_icon="content-copy",
                    on_icon="<i class='mdi mdi-check mr-1'></i>",
                    off_icon="<i class='mdi mdi-close mr-1'></i>",
                    on_text=_("Yes"),
                    off_text=_("No"),
                    del_text=_("Delete"),
                    on_style='success',
                    off_style='secondary',
                    del_style='danger'
                )
            )
        # Only users with high level rights
        if user.has_perm('dashboards.share_dashboard', dash):
            self.fields['explicitViewers'] = MultipleChoiceField(
                required=False,
                widget=TetrasBISelectMultipleWidget(
                    select_all_text=_("Select all"),
                    deselect_all_text=_("Unselect all"),
                    title=_("Select users..."),
                    disabled=disabled,
                    options=options,
                )
            )
            self.fields['explicitViewers'].icon = 'share-variant-outline'
            self.fields['explicitViewers'].label = _("Some users")
            self.fields['explicitViewers'].choices = [(opt['value'], opt['label']) for opt in options]
            self.order_fields(field_order=['public_link', 'allowCreatePublicLink', 'sharedAll', 'explicitViewers', 'sharedWithGuest'])
        # Delete Meta fields for users with low level rights
        if not user.has_perm('dashboards.share_dashboard', dash) and user.has_perm('dashboards.share_dashboard_by_link', dash):
            for f in ['allowCreatePublicLink', 'sharedAll', 'explicitViewers', 'sharedWithGuest']:
                if f in self.fields:
                    del self.fields[f]

    class Meta:
        model = Dashboard
        fields = [
            'allowCreatePublicLink',
            'sharedAll',
            'explicitViewers',
            'sharedWithGuest',
        ]
        labels = {
            'allowCreatePublicLink': _("Allow public link creation"),
            'sharedAll': _("All users"),
            'sharedWithGuest': _("Shared with guest users"),
        }
        icons = {
            'allowCreatePublicLink': 'earth-plus',
            'sharedAll': 'lock-open-variant-outline',
            'sharedWithGuest': 'account-question',
        }
        on_icon = {
            'allowCreatePublicLink': "<i class='mdi mdi-check mr-1'></i>",
            'sharedAll': "<i class='mdi mdi-check mr-1'></i>",
            'sharedWithGuest': "<i class='mdi mdi-check mr-1'></i>",
        }
        off_icon = {
            'allowCreatePublicLink': "<i class='mdi mdi-close mr-1'></i>",
            'sharedAll': "<i class='mdi mdi-close mr-1'></i>",
            'sharedWithGuest': "<i class='mdi mdi-close mr-1'></i>",
        }
        on_text = {
            'allowCreatePublicLink': _("Yes"),
            'sharedAll': _("Yes"),
            'sharedWithGuest': _("Yes"),
        }
        off_text = {
            'allowCreatePublicLink': _("No"),
            'sharedAll': _("No"),
            'sharedWithGuest': _("No"),
        }
        on_style = {
            'allowCreatePublicLink': 'success',
            'sharedAll': 'success',
            'sharedWithGuest': 'success',
        }
        off_style = {
            'allowCreatePublicLink': 'secondary',
            'sharedAll': 'secondary',
            'sharedWithGuest': 'secondary',
        }
        widgets = {}
        for key in ['allowCreatePublicLink', 'sharedAll', 'sharedWithGuest']:
            widgets[key] = TetrasBIToggleSwitchWidget(
                on_icon=on_icon[key],
                off_icon=off_icon[key],
                on_text=on_text[key],
                off_text=off_text[key],
                on_style=on_style[key],
                off_style=off_style[key],
            )
