# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ValidationError
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.utils.crypto import get_random_string
from django.utils.translation import gettext_lazy as _
from hashlib import md5
from itertools import tee
from os import path
from os.path import splitext
from string import ascii_lowercase
from glob import glob
from os.path import basename
from os import walk
from pathlib import Path

from ..dashboardLogger import logger

import re


def getDefaultDashboardTitleFromPath(dash_path):
    return path.splitext(path.basename(dash_path))[0]


def _partition(items, predicate=bool):
    """
    Split in two part an iterable following the predicate

    @param predicate: a func/closure that takes as argument un item and returns a bool

    @returns two iterables, the former contains elements that are not validated by the predicate,
            the latter contains elements that are validated by the predicate
    """
    a, b = tee((predicate(item), item) for item in items)
    return (item for pred, item in a if not pred), (item for pred, item in b if pred)


FileMatchPattern = re.compile('(?!\\.~).*\\.ipynb')
FileExcludePattern = re.compile("(^|.*/)\\.ipynb_checkpoints($|/.*)")
DirExcludePattern = re.compile("(^|.*/)\\..*$")


def is_excluded_files(path):
    return FileExcludePattern.match(path) is not None or FileMatchPattern.match(basename(path)) is None

def is_excluded_dir(path):
    return DirExcludePattern.match(path) is not None


def _getCurrentJupyterDashboards():
    files = _pathFromGlob()
    dirs = _getDirs()
    logger.debug(files)
    logger.debug(dirs)
    result = sorted(list([d for d in files if not is_excluded_files(d)]) +
                         [d for d in dirs if not is_excluded_dir(d)])
    logger.debug(result)
    return result


def _pathFromGlob():
    """ Returns the dashboards paths matching pat """
    return glob('{}/**/*.ipynb'.format(str(settings.DASHBOARD_PATH)), recursive=True)

def _getDirs():
    dirs = [d[0] for d in walk(settings.DASHBOARD_PATH)]
    dirs.remove(settings.DASHBOARD_PATH)
    return dirs


def _getDashboardNamesFromFileSystem():
    return [Path(x).relative_to(settings.DASHBOARD_PATH) for x in _getCurrentJupyterDashboards()]


class DashboardManager(models.Manager):
    """
    Manager of dashboards
    """
    def getPotentialDashboards(self, allow_existing=False, exclude=[]):
        """
        Returns all files and directories that could be dashboards but aren't
        @param exclude : an array of filenames that will ignore all child files and folders
        """
        existing = self.values_list("path", flat=True)
        potentials = [d.as_posix() for d in _getDashboardNamesFromFileSystem() if (allow_existing or d not in existing)]
        excluded_roots = [Path(elt).parent for elt in potentials if any([Path(elt).name == ex for ex in exclude])]
        return [elt for elt in potentials if not any([(ex in Path(elt).parents or ex == Path(elt)) for ex in excluded_roots])]

    def synchronizeDashboards(self, expected_dashboard_paths):
        """
        Synchronize dashboards to expected_dashboard_names list. Then return those dashboards

        @param expected_dashboard_paths: collection of expected dashboard names
        """
        self._removeThoseNotInExpectedDashboards(expected_dashboard_paths)
        self._addThoseNotInExpectedDashboards(expected_dashboard_paths)

    def _removeThoseNotInExpectedDashboards(self, expected_dashboards_paths):
        """
        Remove dashboards in the current_dashboards that are not in expected_dashboards_paths and
        get remaining undeleted dashboards

        @param expected_dashboards_paths: collection of expected dashboards

        @returns Remaining dashboards in the current dashboard collection
        """
        def is_not_in_fs(d):
            return d.path not in [str(p) for p in expected_dashboards_paths]

        remaining_dashboard, not_in_file_fs = _partition(
            self.all(), is_not_in_fs
        )

        for dashboard in not_in_file_fs:
            logger.debug('dashboard to delete : [' + dashboard.path + ']')
            dashboard.delete()

    def _addThoseNotInExpectedDashboards(self, expected_dashboards_paths):
        """
        Add dashboards to the current_dashboards that are not in expected_dashboards_paths,
        then returns current_dashboards

        @param expected_dashboards_paths: collection of expected dashboards

        @returns the current_dashboards updated
        """
        # flat is used because only one field is returned
        current_dashboards_path = self.values_list("path", flat=True)

        def is_not_in_database(d_path):
            return str(d_path) not in current_dashboards_path

        in_database_generator, not_in_database_generator = _partition(
            expected_dashboards_paths, is_not_in_database
        )

        for fs_dashboard_path in not_in_database_generator:
            logger.debug('dashboard to create : [' + str(fs_dashboard_path) + ']')
            new_dashboard = self.createDashboardIfNotExists(fs_dashboard_path,
                                                            getDefaultDashboardTitleFromPath(fs_dashboard_path))
            if new_dashboard is not None:
                new_dashboard.save()

    def createFromPath(self, dashboard_path):
        return self.createDashboardIfNotExists(dashboard_path, getDefaultDashboardTitleFromPath(dashboard_path))

    def createDashboardIfNotExists(self, dashboard_path, dashboard_title):
        """
        create a dashboard if it does not exits.

        @param dashboard_path: the parh of the dashboard
        @param dashboard_title: the title of the dashboard
        """
        dashboard_exists = self.filter(path=dashboard_path).exists()

        if not dashboard_exists:
            logger.debug('create dashboard \'{}\''.format(dashboard_path))
            dashboard = Dashboard(path=dashboard_path, title=dashboard_title)
            logger.debug('created dashboard \'{}\''.format(dashboard_path))
            return dashboard

        return None


def hash_upload(instance, filename):
    """Upload to a unique filenmae
    From https://stackoverflow.com/a/31733919
    """
    instance.img.open()  # make sure we're at the beginning of the file
    contents = instance.img.read()
    fname, ext = splitext(filename)
    return "uploads/dashboards/{0}_{1}{2}".format(fname, md5(contents).digest(), ext)


class Dashboard(models.Model):
    id = models.BigAutoField(primary_key=True)
    path: str = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    title = models.CharField(max_length=255)
    _img = models.FileField(upload_to=hash_upload, blank=True, null=False)
    precomputed = models.BooleanField(default=False)
    sourcesVisible = models.BooleanField(default=True)
    sharedAll = models.BooleanField(default=False)
    allowCreatePublicLink = models.BooleanField(default=False)
    explicitViewers = models.ManyToManyField(User)
    sharedWithGuest = models.BooleanField(default=False)

    objects = DashboardManager()

    def __repr__(self):
        return 'Dashboard(id:{},title:{},precomputed:{},sourcesVisible:{},path:{})'.\
            format(self.id, self.title, self.precomputed, self.sourcesVisible, self.path)

    def clean(self):
        if not (self.path == '--' or self.path.endswith('.ipynb') or self.precomputed):
            raise ValidationError(_('You cannot select a directory if Voilà rendering is enabled.'))


    @property
    def img(self):
        return self._img

    @img.setter
    def img(self, value):
        if self.img is not None:
            self.img.delete(False)
        self._img = value

    # def relative_path(self):
    #     return Path(self.path).relative_to(Path(settings.DASHBOARD_PATH))

    def rename(self, new_name):
        self.path = new_name
        self.save()

    def has_token(self, user):
        """
        check the user has a token for the dashboard

        @param user: the user

        @return true if the user has a token for the dashboard, flase otherwise
        """
        return Token.objects.filter(dashboard=self, user=user).exists()

    def create_token(self, user):
        """
        Create a token for the dashboard for the current user

        @param user: the current user
        """
        Token.create(self, user)

    def load_token(self, user):
        """
        Load the token for the current user

        @param user: the current user
        """
        if self.has_token(user):
            self.token = Token.objects.filter(dashboard=self, user=user).first().token
        else:
            self.token = None

    def delete_token(self, user):
        try:
            Token.objects.get(dashboard=self, user=user).delete()
        except Token.DoesNotExist:
            pass

    def add_explicit_viewers(self, users_ids, user):
        """
        Assign explicitly chosen users (by an admin or data scientist) view rights on the dashboard
        Unassign the other users

        @param users_ids: a list of user ids
        @param user: the user who makes the request
        """
        add_users = User.objects\
            .filter(id__in=users_ids)\
            .exclude(id__in=User.objects.filter(role__guest=1).values('id'))
        remove_users = User.objects.all()\
            .exclude(id__in=add_users.values('id'))\
            .exclude(id__in=User.objects.filter(role__guest=1).values('id'))

        if not user.has_perm('dashboards.update_all_rights'):
            add_users = add_users.filter(id__in=user.role.getUsersBelowMe()).values_list('id', flat=True)
            remove_users = remove_users.filter(id__in=user.role.getUsersBelowMe()).values_list('id', flat=True)

        self.explicitViewers.add(*add_users)
        self.save()

        # If the user has explicit view rights (given previously), he should maintain these rights
        if user in self.explicitViewers.all():
            remove_users = remove_users.exclude(id=user.id)

        self.explicitViewers.remove(*remove_users)
        self.save()


# Delete file on model deletion
@receiver(pre_delete, sender=Dashboard)
def mymodel_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.img.delete(False)


class Token(models.Model):
    """Model to store public tokens in database"""
    class Meta:
        unique_together = ('user', 'dashboard',)

    dashboard = models.ForeignKey(
        Dashboard,
        on_delete=models.CASCADE,
    )
    token = models.CharField(
        max_length=64,
        unique=True,
        primary_key=True
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    def _generateToken(self):
        """Generates a random token that is not in DB"""
        ok = False
        while not ok:
            self.token = get_random_string(6, ascii_lowercase)
            ok = not Token.objects.filter(token=self.token).exists()

    @staticmethod
    def create(dash, user):
        """Creates a token for the dashboard 'path'
        arguments:
            path -- The path of the dashboard
        return PublicToken
        """
        token = Token(dashboard=dash, user=user)
        token._generateToken()
        token.save()
        return token
