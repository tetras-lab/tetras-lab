from django.core.management.base import BaseCommand
from django.conf import settings

from dashboards.dashboardsSynchronizer import DashboardSynchronizer


class Command(BaseCommand):

    def handle(self, *args, **options):
        import logging
        logger = logging.getLogger('dashboardSynchronizer')

        try:
            synchronizer = DashboardSynchronizer(settings.DASHBOARD_PATH)
            synchronizer.start_watchdog()
            logger.debug('dashboards ready')
            synchronizer.wait()
        except Exception as ex:
            logger.warning('dashboard watchdog not launched because '
                           + 'dashboards does not exists in database')
            logger.exception(ex)
            pass
