# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import rules


@rules.predicate
def can_edit(user, dash):
    return not user.is_anonymous and (user.role.data_scientist or user.role.admin)


@rules.predicate
def view_by_id(user, ctx):
    try:
        dash, request = ctx
    except TypeError:
        dash = ctx
    return user is not None and not user.is_anonymous and (
            dash.id in user.dashboard_set.values_list('id', flat=True)
            or (user.role.guest and dash.sharedWithGuest)
            or (not user.role.guest and dash.sharedAll)
        )


@rules.predicate
def can_share_public_link(user, ctx):
    # This try-except allows to use two rules with distinct signatures
    try:
        dash, request = ctx
    except TypeError:
        dash = ctx
    return dash.allowCreatePublicLink and not user.role.guest


@rules.predicate
def view_by_token(user, ctx):
    dash, request = ctx
    return request.session.get('dashid', None) == dash.id


@rules.predicate
def update_all_rights_on_dashboard(user, ctx):
    return user.role.admin


# Permissions
rules.add_perm("dashboards.add_dashboard", can_edit)
rules.add_perm("dashboards.update_dashboard", can_edit)
rules.add_perm("dashboards.delete_dashboard", can_edit)
rules.add_perm("dashboards.view_dashboard", view_by_id | can_edit)
rules.add_perm("dashboards.render_dashboard", view_by_id | view_by_token | can_edit)
rules.add_perm("dashboards.share_dashboard", can_edit)
rules.add_perm("dashboards.share_dashboard_by_link", can_share_public_link)
rules.add_perm("dashboards.access_share_dashboard_form", can_share_public_link | can_edit)
rules.add_perm("dashboards.update_all_rights", update_all_rights_on_dashboard)
