from django.apps import AppConfig


class DashboardsConfig(AppConfig):
    name = 'dashboards'
    synchronizer = None

    def ready(self):
        AppConfig.ready(self)
        pass
