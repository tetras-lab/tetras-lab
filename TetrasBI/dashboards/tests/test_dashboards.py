import pytest
from django.test.testcases import TestCase
from django.test import Client
from django.urls import reverse

from dashboards.models.dashboard import Dashboard, DashboardManager
from .test_HttpResponseStatus import HttpResponseStatus
from .test_factories import default_fs, create_user, test_password, default_db


def test_dashboardManageSynchronizeDashboard_should_synchronize_database_to_those_in_file_system(
        default_db: DashboardManager, default_fs) -> None:
    """
    getSynchronizedDashboards should set dashboards in the database equivalent to the file system dashboards
    """
    # fs_path = list([d.absolute().str(default_fs) for d in default_fs])
    default_db.synchronizeDashboards(default_fs)
    assert Dashboard.objects.count() == len(default_fs)


def test_dashboardManagerSynchronizeDashboard_should_not_remove_from_database_existing_elements_if_in_file_system(
        default_db: DashboardManager,
        default_fs) -> None:
    """
    synchronizedDashboards should set dashboards in the database equivalent to the file system dashboards
    """

    assert Dashboard.objects.count() == default_db.count()

    # init database
    original_dashboards = list(default_db.all())

    # prepare first element in database
    first_dashboard = original_dashboards[0]
    first_dashboard.description = "myDescription"
    # first_dashboard.save()
    Dashboard.objects.filter(path=first_dashboard.path).update(description=first_dashboard.description)

    # run test function
    default_db.synchronizeDashboards([str(d) for d in default_fs])
    assert Dashboard.objects.count() == len(default_fs)

    # check expected element hasn't been delete or added
    element = Dashboard.objects.get(path=first_dashboard.path)
    assert element.description == first_dashboard.description


@pytest.mark.django_db(transaction=True)
def test_view(client, create_user, test_password):
    user = create_user()
    client.login(username=user.username, password=test_password)
    response = client.get('/dashboard/')
    assert response.status_code == HttpResponseStatus.STATUS_OK


class TestDashboards(TestCase):
    """
    Dashboards tests on django project side.
    @warning : TestCase should be imported from django.test.testcases instead of unittest.TestCase
    """

    # The fixture should be created using :
    # './manage.py dumpdata TetrasBI.TetrasBI.models.dashboard.OldDashboard > dashboard.json'
    fixtures = ['dashboard.json']

    clientUser = None
    clientSuperUser = None
    clientUserPwd = None

    def test_unauthenticated_user_that_go_to_account_page_should_be_redirected(self) -> None:
        """
        Test that account page can be accessed by anonymous user
        """
        c = Client()
        response = c.post(reverse('account'))
        assert response.status_code == HttpResponseStatus.STATUS_REDIRECTED
        # print(response)

    def test_dashboard_page_requires_authentication(self) -> None:
        """
        Test that dashboards page requires user authentication
        """
        c = Client()
        response = c.post(reverse('home'))
        assert response.status_code == HttpResponseStatus.STATUS_REDIRECTED

    def test_unauthenticated_user_that_go_to_jupyterLabe_page_should_be_redirected_once(self) -> None:
        """
        Test that jupyter lab access requires user authentication
        """
        c = Client()
        response = c.post('/admin/jupyter', follow=True)
        assert response.status_code == HttpResponseStatus.STATUS_OK
        assert len(response.redirect_chain) != 0
