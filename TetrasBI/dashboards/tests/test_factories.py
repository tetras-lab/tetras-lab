import uuid
import pytest
import shutil
import time

from pathlib import Path
from django.conf import settings
from dashboards.models.dashboard import Dashboard
from dashboards.dashboardsSynchronizer import DashboardSynchronizer

TEST_FS_DASHBOARD_DIRECTORY = settings.DASHBOARD_PATH
_test_fs_dashboard_directory = Path(TEST_FS_DASHBOARD_DIRECTORY)


@pytest.fixture()
def default_fs_directory():
    return TEST_FS_DASHBOARD_DIRECTORY


def create_python_notebook_file_path(file_name):
    return _test_fs_dashboard_directory.joinpath(file_name + '.ipynb')


@pytest.fixture
def default_db(db):
    """
        Default databases dashboards names for synchronization test
    """
    dash = Dashboard.objects.createFromPath(create_python_notebook_file_path('Bokeh'))
    dash.description = 'Bokeh description'
    dash.save()
    Dashboard.objects.createFromPath(create_python_notebook_file_path('MonetDB'))
    dash.description = 'MonetDB description'
    dash.save()
    Dashboard.objects.createFromPath(create_python_notebook_file_path('csv'))
    dash.description = 'csv description'
    dash.save()
    Dashboard.objects.createFromPath(create_python_notebook_file_path('NoInFileSystem'))
    dash.save()

    return Dashboard.objects


def create_synchronizer(watched_directory):
    directory_path = Path(watched_directory)
    if directory_path.exists() and not directory_path.is_dir():
        directory_path.unlink()

    if directory_path.exists() and directory_path.is_dir():
        shutil.rmtree(directory_path)

    if not directory_path.exists():
        directory_path.mkdir(parents=True, exist_ok=True)

    result = DashboardSynchronizer(watched_directory)
    return result


@pytest.fixture
def default_fs_infos():
    """
    Default file system infos dashboards names for synchronization test
    """

    synchronizer = create_synchronizer(TEST_FS_DASHBOARD_DIRECTORY)
    default_fs = [create_python_notebook_file_path('Bokeh'),
                  create_python_notebook_file_path('MonetDB'),
                  create_python_notebook_file_path('csv'),
                  create_python_notebook_file_path('NewProject'),
                  create_python_notebook_file_path('dashboardWithToken')]
    result = FsInfos(synchronizer, _test_fs_dashboard_directory, default_fs)
    for fileName in default_fs:
        file_path = result.to_absolute_path(fileName)
        file_path.touch()

    time.sleep(2)
    return result


@pytest.fixture
def default_fs():
    """
    Default file system dashboards names for synchronization test
    """
    default_fs = [create_python_notebook_file_path('Bokeh'),
                  create_python_notebook_file_path('MonetDB'),
                  create_python_notebook_file_path('csv'),
                  create_python_notebook_file_path('NewProject'),
                  ]

    return default_fs


@pytest.fixture
def test_password():
    """
    Create fake password for test user
    """
    return 'strong-test-pass'


@pytest.fixture
def create_user(db, django_user_model, test_password):
    """
    Create user for a database
    @param db : the database
    @param django_user_model : django user model for user registration
    @param test_password : the password for the user
    """

    def make_user(**kwargs):
        kwargs['password'] = test_password
        if 'username' not in kwargs:
            kwargs['username'] = str(uuid.uuid4())
        return django_user_model.objects.create_user(**kwargs)

    return make_user


class FsInfos:
    watchdog = None
    expected_files = None
    fs_directory = None

    # For call to repr(). Prints object's information
    def __repr__(self):
        return 'FsInfos(%s, %s)' % (self.fs_directory, self.expected_files)

        # For call to str(). Prints readable form

    def __str__(self):
        return '%s + i%s' % (self.fs_directory, self.expected_files)

    def __init__(self, watchdog, fs_directory, expected_files):
        if watchdog is None:
            raise ValueError('watchdog should given')
        if expected_files is None:
            raise ValueError('')
        self.watchdog = watchdog
        self.expected_files = expected_files
        self.fs_directory = fs_directory

    def to_absolute_path(self, dashboard_name):
        return self.fs_directory.joinpath(dashboard_name)

    def _path_relative_to_fs_directory(self, path):
        return path.relative_to(self.fs_directory)

    def getDashboard(self, path):
        return Dashboard.objects.get(path=self._path_relative_to_fs_directory(path))

    def start_watchdog(self):
        self.watchdog.start_watchdog()

    def stop_watchdog(self):
        self.watchdog.start_watchdog()
