import pytest

from .test_factories import default_db, default_fs_infos

import time
from django.conf import settings
from os import path, rename
from pathlib import Path

from dashboards.models.dashboard import Dashboard, DashboardManager
from dashboards.dashboardLogger import logger


@pytest.mark.django_db(transaction=True)
@pytest.mark.skip(reason="synchronize is no longer used")
def test_dashboardSynchronizer_should_rename_dashboard_on_moved_file(default_fs_infos):
    # Prepare test
    expected_description = "My description"
    new_expected_file_path = default_fs_infos.to_absolute_path("myNewDashboard.ipynb")

    default_fs_infos.start_watchdog()

    first_dashboard = Dashboard.objects.first()
    assert first_dashboard is not None

    first_dashboard.description = expected_description
    first_dashboard.save()


    # do action
    logger.debug('dashboard moved from test : ' + first_dashboard.path)
    rename(str(default_fs_infos.to_absolute_path(first_dashboard.path)), str(default_fs_infos.to_absolute_path(new_expected_file_path)))
    time.sleep(1)

    # check that the dashboard has been renamed
    updated_dashboard = default_fs_infos.getDashboard(new_expected_file_path)

    assert updated_dashboard is not None
    assert updated_dashboard.id == first_dashboard.id
    assert updated_dashboard.description == expected_description
    default_fs_infos.stop_watchdog()
