from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

from dashboards.models.dashboard import Dashboard, is_excluded_files, _getDashboardNamesFromFileSystem
from dashboards.dashboardLogger import logger


class DashboardSynchronizer:
    """
    Dashboards synchronizer is watchdog for directories and files changes.
    """
    _observer = None
    _fs_watcher: PatternMatchingEventHandler = None
    _rootPath = None
    _is_watchdog_started = False

    _jupiter_lab_temp_file_root = None

    class _DashboardFsWatcher(PatternMatchingEventHandler):

        _dashboard_manager = Dashboard.objects
        _rootPath = None

        def __init__(self, root_path: Path):
            PatternMatchingEventHandler.__init__(self, ["*.ipynb"], ["**/.ipynb_checkpoints/**;"], True, False)
            self._rootPath = root_path

        def process(self, event):
            """
            event.event_type
                'modified' | 'created' | 'moved' | 'deleted'
            event.is_directory
                True | False
            event.src_path
                path/to/observed/file
            """
            return

        def on_moved(self, event):
            if not event.is_directory and is_excluded_files(event.src_path):
                return

            logger.debug('watchdog dashboard move detected')
            try:
                logger.debug(
                    'dashboard move from \'{}\' to \'{}\' detected'.format(event.src_path, event.dest_path))

                # logger.debug('all dashboards in database : {}'.format(Dashboard.objects.all()))
                src_path = Path(event.src_path).relative_to(self._rootPath)
                dest_path = Path(event.dest_path).relative_to(self._rootPath)

                dashboard = Dashboard.objects.get(path=str(src_path))
                if (dashboard):
                    dashboard.path = dest_path
                    dashboard.save()
                    logger.debug('dashboard moved from \'{}\' to \'{}\' completed'
                                 .format(src_path, dest_path))
                else:
                    logger.debug('watchdog : '+src_path + ' is not a known dashboard')
            except Dashboard.DoesNotExist:
                logger.debug('dashboard moved can''t be moved (\'{}\' does not exists)'.format(event.src_path))
                pass

    def __init__(self, base_path: str):

        self._is_watchdog_started = False
        self._rootPath = Path(base_path)
        self._jupiter_lab_temp_file_root = self._rootPath.joinpath(".ipynb_checkpoints")
        self._fs_watcher = DashboardSynchronizer._DashboardFsWatcher(base_path)

    def _synchronizeDatabaseToFileSystemDashboards(self):
        if self._rootPath is None:
            return
        logger.debug("initialize dashboards synchronizer")
        logger.debug("initialize dashboards from fs : '{}'".format(self._rootPath))
        self.synchronizedDashboardsFromFsName(
            _getDashboardNamesFromFileSystem())

    def _createFsObserver(self):
        logger.debug("dashboard fs observer [Starting]")
        self._observer = Observer()
        self._observer.schedule(self._fs_watcher, path=self._rootPath, recursive=True)
        self._observer.start()
        logger.debug("dashboard fs observer [started]")

    def start_watchdog(self):
        """
        start directory watch. Start update of database registered dashboards from file system modification

        @warning if one file has been modified (moved or renamed) while the
                 watchdog hasn't been started, then the dashboard will be deleted
                 from the database and a new one will be created within the database.
        """
        if self._is_watchdog_started:
            return

        if self._rootPath is None:
            return
        # self._synchronizeDatabaseToFileSystemDashboards()
        self._createFsObserver()
        self._is_watchdog_started = True

    def wait(self):
        try:
            while self._observer.is_alive():
                self._observer.join(1)
        except KeyboardInterrupt:
            logger.debug("keyboard interruption detected")
            pass

    def stop_watchdog(self):
        if not self._is_watchdog_started:
            return
        if self._observer.is_alive():
            self._observer.stop()
        self._observer.join()
        self._is_watchdog_started = False

    def synchronizedDashboardsFromFsName(self, expected_dashboard_names) -> list:
        """
        Synchronize dashboards to expected_dashboard_paths list.

        @param expected_dashboard_names: collection of expected dashboard names.
        """
        Dashboard.objects.synchronizeDashboards(expected_dashboard_names)
