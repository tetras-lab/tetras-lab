# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.models import User, Permission
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.http import Http404, HttpResponse, FileResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext as _
from django.views.decorators.http import require_http_methods
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from rules.contrib.views import PermissionRequiredMixin
from pathlib import Path

from TetrasBI.models import Role
from TetrasBI.views.renderer import renderDashboard

from .models.dashboard import Dashboard, Token
from .forms import DashboardEditForm, DashboardShareForm


class DashboardListView(LoginRequiredMixin, ListView):
    login_url = '/accounts/login/'
    redirect_field_name = 'login'
    model = Dashboard
    template_name = 'dashboards/index.html'
    form = None

    def get_queryset(self):
        dashboards = Dashboard.objects.order_by('title')
        user = self.request.user
        for d in dashboards:
            if user.has_perm('dashboards.add_dashboard', d):
                d.load_token(user)
        return [x for x in dashboards if user.has_perm('dashboards.view_dashboard', x)]

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        scroll = None

        if self.request.session.get('dashboard'):
            scroll = '#{}'.format(self.request.session['dashboard'])
            del self.request.session['dashboard']

        context['scroll'] = scroll

        return context


class DashboardCreateView(PermissionRequiredMixin, CreateView):
    model = Dashboard
    permission_required = 'dashboards.add_dashboard'
    template_name = 'dashboards/_edit_modal.html'
    form_class = DashboardEditForm
    status = 'error'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['status'] = self.status
        context['dashboard'] = None
        return context

    def form_valid(self, form):
        dash = form.save()
        self.status = 'ok'
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _('Dashboard {dash_title} successfully created').format(dash_title=dash.title)
        )
        self.request.session['dashboard'] = dash.id
        return self.render_to_response(self.get_context_data(form=form))


class DashboardUpdateView(PermissionRequiredMixin, UpdateView):
    model = Dashboard
    permission_required = 'dashboards.update_dashboard'
    template_name = 'dashboards/_edit_modal.html'
    form_class = DashboardEditForm
    status = 'error'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['status'] = self.status
        return context

    def form_valid(self, form):
        dash = form.save()
        self.status = 'ok'
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _('Dashboard {dash_title} successfully updated').format(dash_title=dash.title)
        )
        self.request.session['dashboard'] = dash.id
        return self.render_to_response(self.get_context_data(form=form))


class DashboardDeleteView(PermissionRequiredMixin, DeleteView):
    model = Dashboard
    permission_required = 'dashboards.delete_dashboard'
    success_url = reverse_lazy('dashboard')

    def delete(self, request, *args, **kwargs):
        dash = _getFromIdOrRaise404(kwargs['pk'])
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _('Dashboard {dash_title} successfully deleted').format(dash_title=dash.title)
        )
        return super().delete(self, request, *args, **kwargs)


class DashboardShareView(PermissionRequiredMixin, UpdateView):
    model = Dashboard
    permission_required = 'dashboards.access_share_dashboard_form'
    template_name = 'dashboards/_share_modal.html'
    form_class = DashboardShareForm
    status = 'error'

    def get_initial(self):
        initial = super().get_initial()
        user = self.request.user
        dash = _getFromIdOrRaise404(self.kwargs['pk'])
        try:
            initial['token'] = Token.objects.get(dashboard_id=dash.id, user_id=user.id).token
        except Token.DoesNotExist:
            initial['token'] = ''
        return initial

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super().get_form_kwargs(**kwargs)
        dash = _getFromIdOrRaise404(self.kwargs['pk'])
        form_kwargs['dash'] = dash
        user = self.request.user
        form_kwargs['user'] = user
        # Token base url
        form_kwargs['base_url'] = f"{self.request.scheme}://{self.request.get_host()}/dashboard/public/"
        # Token value
        try:
            form_kwargs['token'] = Token.objects.get(dashboard_id=dash.id, user_id=user.id).token
        except Token.DoesNotExist:
            form_kwargs['token'] = ''
        # Disable multiple user selection if allUsers checkbox is checked
        form_kwargs['disabled'] = dash.sharedAll
        # Options for multiple user selection
        form_kwargs['options'] = self.get_select_multiple_options()
        return form_kwargs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['status'] = self.status
        return context

    def form_valid(self, form):
        user = self.request.user
        dash = _getFromIdOrRaise404(self.kwargs['pk'])

        if not (user.has_perm('dashboards.share_dashboard', dash)
                or user.has_perm('dashboards.share_dashboard_by_link', dash)):
            raise PermissionDenied(_('Not allowed'))

        public_link = form.cleaned_data['public_link']
        if public_link and not dash.has_token(user):
            dash.create_token(user)
            messages.add_message(self.request, messages.SUCCESS, _('Token created'))
        elif not public_link and dash.has_token(user):
            dash.delete_token(self.request.user)
            messages.add_message(self.request, messages.SUCCESS, _('Token deleted'))

        if user.has_perm('dashboards.share_dashboard', dash):
            dash.sharedAll = form.cleaned_data['sharedAll']
            dash.allowCreatePublicLink = form.cleaned_data['allowCreatePublicLink']
            dash.sharedWithGuest = form.cleaned_data['sharedWithGuest']
            if not dash.sharedAll:
                dash.add_explicit_viewers(
                    [User.objects.get(username=u).id for u in form.cleaned_data['explicitViewers']],
                    user
                )

        dash.save()

        if user.has_perm('dashboards.share_dashboard', dash):
            if Dashboard.objects.filter(sharedWithGuest=1).count() == 0:
                User.objects.filter(role__guest=1).delete()
            else:
                Role.create_or_get_guest_user()

        self.status = 'ok'
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _('Dashboard {dash_title} successfully updated').format(dash_title=dash.title)
        )
        self.request.session['dashboard'] = dash.id
        return self.render_to_response(self.get_context_data(form=form))

    def get_select_multiple_options(self):
        user = self.request.user
        guests_id = list(User.objects.filter(role__guest=1).values_list('id', flat=True))
        users = User.objects.exclude(id__in=[user.id]+guests_id).order_by('username').all()
        values = [u.username for u in users]
        labels = [u.first_name+' '+u.last_name if len(u.first_name+u.last_name) else u.username for u in users]
        can_manage_users = user.has_perm('dashboards.update_all_rights')
        disabled = ['disabled' if (not can_manage_users and (u.role.data_scientist or u.role.admin)) else '' for u in users]
        return [{'value': v, 'label': l, 'disabled': d} for (v, l, d) in zip(values, labels, disabled)]


def _getFromIdOrRaise404(dashboard_id):
    try:
        return Dashboard.objects.get(id=dashboard_id)
    except ObjectDoesNotExist:
        raise Http404(_('Dashboard %(id)s does not exists') % {'id': id})


@login_required
@require_http_methods(["GET"])
def DashboardRendering(request, dash_id):
    dash = _getFromIdOrRaise404(dash_id)
    user = request.user
    if not user.has_perm('dashboards.view_dashboard', dash):
        raise PermissionDenied(_('Not allowed'))
    return renderDashboard(request, dash_id)


def getMediaFile(request, dash_id, path):
    """Return the media file for the given dashboard and path
    raise Http404, PermissionDenied
    return file contents
    """
    dash = _getFromIdOrRaise404(dash_id)
    user = request.user
    if not user.has_perm('dashboards.render_dashboard', (dash, request)):
        raise PermissionDenied(_('Dashboard not allowed'))

    root = Path(f'{settings.DASHBOARD_PATH}/media/{dash_id}').resolve()
    child = Path(f'{root.as_posix()}/{path}').resolve()
    if root not in child.parents:
        # Someone is trying to acces a file out of media path
        raise PermissionDenied(_('Path not allowed'))

    if child.is_dir():
        raise PermissionDenied(_('Accessing directories is not allowed'))

    if not child.exists():
        raise Http404(_('File "%(path)s" not found for dashboard "%(title)s"') % {'path': path, 'title': dash.title})

    return FileResponse(open(child.as_posix(), 'rb'), as_attachment=True, filename=child.name)


@require_http_methods(["GET"])
def guestLogin(request):
    user = request.user
    if not user.has_perm('TetrasBI.login_as_guest', user):
        raise PermissionDenied(_('Not allowed'))
    guest = Role.create_or_get_guest_user()
    if guest is not None:
        login(request, guest, backend='django.contrib.auth.backends.ModelBackend')
    return redirect('dashboard')
