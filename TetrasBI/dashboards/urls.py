# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""TetrasBI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, re_path
from .views import DashboardListView, DashboardCreateView, DashboardUpdateView, DashboardDeleteView, DashboardShareView, \
    DashboardRendering, getMediaFile, guestLogin

urlpatterns = [
    # Dashboards views
    path('', DashboardListView.as_view(), name='dashboard'),
    path('guest', guestLogin, name='guestLogin'),
    path('add', DashboardCreateView.as_view(), name='addDashboard'),
    path('<int:pk>/update', DashboardUpdateView.as_view(), name='updateDashboard'),
    path('<int:pk>/delete', DashboardDeleteView.as_view(), name='deleteDashboard'),
    path('<int:pk>/share', DashboardShareView.as_view(), name='shareDashboard'),
    # Dashboards rendering
    path('<int:dash_id>', DashboardRendering, name='dashboardRendering'),
    re_path(r'^(?P<dash_id>[0-9]*)/media/(?P<path>.*)$', getMediaFile, name='get_media_file'),
]
