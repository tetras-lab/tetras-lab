# Generated by Django 3.2.14 on 2022-09-26 11:49

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboards', '0004_dashboard_recreate_dashboardModel'),
    ]

    operations = [
        migrations.AddField(
            model_name='dashboard',
            name='explicitViewers',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='dashboard',
            name='sharedAll',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='dashboard',
            name='allowCreatePublicLink',
            field=models.BooleanField(default=False),
        )
    ]
