# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import rules
from django.contrib.auth.models import User
from dashboards.models.dashboard import Dashboard


@rules.predicate
def access_jupyter(user):
    return user.has_perm('dashboards.add_dashboard')


@rules.predicate
def is_anonymous(user):
    return user.is_anonymous


@rules.predicate
def is_tl_admin(user):
    return not is_anonymous(user) and user.role.admin


@rules.predicate
def is_tl_data_scientist(user):
    return not is_anonymous(user) and user.role.data_scientist


@rules.predicate
def is_not_installed(user):
    return not User.objects.exists()


@rules.predicate
def is_shared_account(user):
    return not is_anonymous(user) and user.role.shared_account


@rules.predicate
def is_guest(user):
    return not is_anonymous(user) and user.role.guest


@rules.predicate
def is_allowed_to_login_as_guest(user):
    return is_anonymous(user) and Dashboard.objects.filter(sharedWithGuest=1).count() > 0


def can_impersonate(request):
    return is_tl_admin(request.user)


rules.add_perm("TetrasBI.access_voila", rules.always_allow)
rules.add_perm("TetrasBI.access_jupyter", access_jupyter)
rules.add_perm("TetrasBI.access_nbviewer", rules.always_allow)
rules.add_perm("TetrasBI.access_webvowl", rules.is_authenticated)

rules.add_perm("TetrasBI.manage_users", is_tl_admin)
rules.add_perm("TetrasBI.administrate_platform", is_tl_admin)
rules.add_perm("TetrasBI.create_first_user", is_not_installed)
rules.add_perm("TetrasBI.manage_self_account", ~is_anonymous & ~is_shared_account)
rules.add_perm("TetrasBI.login_as_guest", is_allowed_to_login_as_guest)

rules.add_perm("TetrasBI.add_job_definition", is_tl_admin | is_tl_data_scientist)
rules.add_perm("TetrasBI.update_job_definition", is_tl_admin | is_tl_data_scientist)
rules.add_perm("TetrasBI.delete_job_definition", is_tl_admin | is_tl_data_scientist)
rules.add_perm("TetrasBI.view_job_definition", is_tl_admin | is_tl_data_scientist)
rules.add_perm("TetrasBI.add_job", is_tl_admin | is_tl_data_scientist)
rules.add_perm("TetrasBI.update_job", is_tl_admin | is_tl_data_scientist)
rules.add_perm("TetrasBI.delete_job", is_tl_admin | is_tl_data_scientist)
rules.add_perm("TetrasBI.view_job", is_tl_admin | is_tl_data_scientist)
