# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext_lazy as _, activate as set_lang
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordResetView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from rules.contrib.views import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied

import logging
from ..forms.users import UserInitialCreationForm, UserEditForm, UserEditSelfForm
from ..forms.registration import PasswordChangeForm
from .multipleforms import MultipleFormsView
from ..models.lang import Lang

logger = logging.getLogger(__name__)


class NotSharedAccountUserMixin(object):
    def has_permissions(self):
        return self.request.user.has_perm('TetrasBI.manage_self_account')

    def dispatch(self, request, *args, **kwargs):
        if not self.has_permissions():
            raise PermissionDenied(_('Not allowed'))
        return super().dispatch(request, *args, **kwargs)


class EditAccountView(NotSharedAccountUserMixin, MultipleFormsView, LoginRequiredMixin):
    template_name = 'users/account.html'
    form_classes = {
        'user': UserEditSelfForm,
        'pwd': PasswordChangeForm,
    }
    success_messages = {
        'user': _('Profile updated'),
        'pwd': _('Password updated'),
    }

    def get_initial_form_kwargs(self, key):
        argname = 'user' if key == 'pwd' else 'instance'
        return {argname: self.request.user}

    def form_valid(self, key, form):
        form.save()
        update_session_auth_hash(self.request, self.request.user)
        if key == 'user':
            user = self.request.user
            lang = form.cleaned_data['lang']
            set_lang(lang)
            try:
                user.lang.lang = lang
                user.lang.save()
            except AttributeError:
                Lang.objects.create(user=user, lang=lang)
        messages.success(self.request, self.success_messages[key])


class UserCreateFirstView(PermissionRequiredMixin, CreateView):
    model = User
    form_class = UserInitialCreationForm
    template_name = 'users/firstrun.html'
    success_url = reverse_lazy('home')
    permission_required = 'TetrasBI.create_first_user'

    def form_valid(self, form):
        response = super().form_valid(form)
        u = User.objects.first()
        u.role.admin = True
        u.role.data_scientist = True
        u.role.save()
        messages.success(self.request, _('User {u_username} created').format(u_username=u.username))
        return response


class UsersView(PermissionRequiredMixin, ListView):
    template_name = 'users/index.html'
    ordering = 'username'
    model = User
    permission_required = 'TetrasBI.manage_users'
    form = None

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['form'] = UserEditForm() if self.form is None else self.form
        context['open_form'] = self.form is not None
        return context

    def post(self, request):
        return self.get(request)


class UserResetPwView(PermissionRequiredMixin, UpdateView):
    model = User
    permission_required = 'TetrasBI.manage_users'

    def post(self, request, pk, *args, **kwargs):
        PasswordResetView.as_view()(request, *args, **kwargs)
        user = User.objects.get(pk=pk)
        messages.success(request, _('Password reset link sent for user {user_username}').format(user_username=user.username))
        return redirect(reverse_lazy('users'))


class UserModifyView(PermissionRequiredMixin):
    model = User
    permission_required = 'TetrasBI.manage_users'
    template_name = 'users/_edit_modal.html'
    form_class = UserEditForm
    status = 'error'
    verb = _('created')

    def updateRoles(self, user, form):
        roles = form.cleaned_data['role']
        user.role.admin = 'admin' in roles
        user.role.data_scientist = 'data_scientist' in roles
        user.role.shared_account = 'shared_account' in roles
        user.role.save()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['status'] = self.status
        return context

    def form_valid(self, form):
        user = form.save()
        self.status = 'ok'
        self.updateRoles(user, form)
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _('User {user_username} succesfully {verb}').format(user_username=user.username, verb=self.verb)
        )
        return self.render_to_response(self.get_context_data(form=form))


class UserCreateView(UserModifyView, CreateView):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['user'] = None
        return context


class UserUpdateView(UserModifyView, UpdateView):
    verb = _('updated')

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super().get_form_kwargs(**kwargs)
        modified_user = User.objects.get(id=self.kwargs['pk'])
        # get associated roles
        form_kwargs['initial_roles'] = [r for r in modified_user.role.ROLES
                                        if getattr(modified_user.role, r) and r != 'guest']
        return form_kwargs


class UserDeleteView(UserModifyView, DeleteView):
    success_url = reverse_lazy('users')

    def delete(self, request, *args, **kwargs):
        user = User.objects.get(pk=self.kwargs['pk'])
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _('User {user_username} successfully deleted').format(user_username=user.username)
        )
        return super().delete(self, request, *args, **kwargs)
