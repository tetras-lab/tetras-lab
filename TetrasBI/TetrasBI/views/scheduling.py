# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied, ValidationError
from django.shortcuts import redirect
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import FormView
from django.utils.translation import gettext as _, get_language

from rules.contrib.views import PermissionRequiredMixin

from TetrasBI.models.upstream import UpstreamFactory

from ..forms.scheduling import SchedulingEditJobDefinition, SchedulingEditJob, SchedulingCreateJob
from ..models.scheduling import Scheduling


class SchedulingFactory:
    _scheduling = None

    @property
    def scheduling(self):
        if self._scheduling is None:
            self._scheduling = Scheduling(
                UpstreamFactory.getUpstream('jupyter').apiCall("read", "job_definitions"),
                UpstreamFactory.getUpstream('jupyter').apiCall("read", "jobs")
            )
        return self._scheduling


class SchedulingListView(PermissionRequiredMixin, TemplateView, SchedulingFactory):
    permission_required = 'TetrasBI.view_job_definition'
    template_name = 'scheduling/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        scroll = None
        self.scheduling.make_time_and_cron_human_readable(get_language())
        context['jobs_and_defs'] = self.scheduling.job_defs
        if self.request.session.get('job_def'):
            scroll = '#{}'.format(self.request.session['job_def'])
            del self.request.session['job_def']
        context['scroll'] = scroll
        return context


class SchedulingCreateJobDefView(PermissionRequiredMixin, FormView):
    permission_required = 'TetrasBI.add_job_definition'
    template_name = 'scheduling/_edit_modal.html'
    form_class = SchedulingEditJobDefinition
    object = 'job_definitions'
    status = 'error'
    hidden_fields = {
        "runtime_environment_name": "python",
        "output_filename_template": "{{input_filename}}-{{create_time}}",
        "timezone": "Europe/Paris",
        "output_formats": ["html"]
    }
    action = 'create'
    validation_error_message = _('Create job definition API failed: ')
    validation_success_message = _('Job definition {job_def_name} successfully created')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'create_or_update': self.action})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['status'] = self.status
        context['job_def'] = None
        return context

    def get_initial(self):
        initial = super().get_initial()
        for field in self.hidden_fields:
            initial[field] = self.hidden_fields[field]
        return initial

    def form_valid(self, form):
        user = self.request.user
        if not (user.has_perm(self.permission_required)):
            raise PermissionDenied(_('Not allowed'))
        form.cleaned_data = Scheduling.format_job_def_post_data(form.cleaned_data)
        if self.kwargs.get('id', ''):
            form.cleaned_data['id'] = self.kwargs['id']
        response = UpstreamFactory.getUpstream('jupyter').apiCall(self.action, self.object, payload=form.cleaned_data)
        if response.get("message", ""):
            form.add_error(None, ValidationError(self.validation_error_message + response.get("message", ""), code='failure'))
            return super().form_invalid(form)
        self.status = 'ok'
        messages.add_message(self.request, messages.SUCCESS, self.validation_success_message.format(job_def_name=form.cleaned_data["name"]))
        self.request.session['job_def'] = self.kwargs['id'] if self.kwargs.get('id', '') else response['job_definition_id']
        return self.render_to_response(self.get_context_data(form=form))


class SchedulingUpdateJobDefView(SchedulingCreateJobDefView, SchedulingFactory):
    permission_required = 'TetrasBI.add_job_definition'
    template_name = 'scheduling/_edit_modal.html'
    form_class = SchedulingEditJobDefinition
    object = 'job_definitions'
    status = 'error'
    action = 'update'
    validation_error_message = _('Update job definition API failed: ')
    validation_success_message = _('Job definition {job_def_name} successfully updated')

    def get_initial(self):
        initial = super().get_initial()
        job_def = self.scheduling.get_job_def(self.kwargs['id'])
        for field in [f for f in self.get_form_class().form_fields]:
            initial[field] = Scheduling.initialize(field, job_def)
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['status'] = self.status
        context['job_def'] = self.scheduling.get_job_def(self.kwargs['id'])
        return context


class SchedulingDeleteJobDefView(PermissionRequiredMixin, View):
    object = 'job_definitions'
    permission_required = 'TetrasBI.delete_job_definition'
    validation_error_message = _('Job definition cannot be deleted!')
    validation_success_message = _('Job definition successfully deleted')

    def post(self, request, *args, **kwargs):
        user = self.request.user
        if not (user.has_perm(self.permission_required)):
            raise PermissionDenied(_('Not allowed'))
        response = UpstreamFactory.getUpstream("jupyter").apiCall("delete", self.object, payload={"id": self.kwargs['id']})
        if response.get("message", ""):
            messages.add_message(self.request, messages.ERROR, self.validation_error_message + response["message"])
        else:
            messages.add_message(self.request, messages.SUCCESS, self.validation_success_message)
        return redirect('scheduling')


class SchedulingRunNowJobDefView(SchedulingUpdateJobDefView):
    permission_required = 'TetrasBI.add_job_definition'
    template_name = 'scheduling/_edit_modal.html'
    form_class = SchedulingCreateJob
    object = 'jobs'
    status = 'error'
    action = 'create'
    validation_error_message = _('Create job API failed: ')
    validation_success_message = _('Job successfully run')

    def get_initial(self):
        initial = super().get_initial()
        initial['job_definition_id'] = self.kwargs['id']
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['job_action'] = 'run'
        return context


class SchedulingReadJobView(PermissionRequiredMixin, FormView, SchedulingFactory):
    permission_required = 'TetrasBI.view_job'
    template_name = 'scheduling/_edit_job_modal.html'
    form_class = SchedulingEditJob
    status = 'error'

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super().get_form_kwargs(**kwargs)
        UpstreamFactory.getUpstream("jupyter").apiCall("export_html", "jobs", payload={"id": self.kwargs['id']})
        html = self.scheduling.get_html(self.kwargs['id'])
        form_kwargs['html_output'] = html
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['status'] = self.status
        self.scheduling.make_time_and_cron_human_readable(get_language())
        context['job'] = self.scheduling.get_job(self.kwargs['id'])
        return context


class SchedulingDeleteJobView(SchedulingDeleteJobDefView):
    object = 'jobs'
    permission_required = 'TetrasBI.delete_job'
    validation_error_message = _('Job cannot be deleted!')
    validation_success_message = _('Job successfully deleted')
