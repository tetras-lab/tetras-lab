# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from channels.exceptions import DenyConnection
from channels.generic.websocket import AsyncWebsocketConsumer
import asyncio
import websockets
import logging

logger = logging.getLogger(__name__)


class BaseWebsocketProxy(AsyncWebsocketConsumer):
    """Base class for proxying websocket connections.
        Adapted from https://gist.github.com/brianglass/e3184341afe63ed348144753ee62dce5
    """

    # This is the frequency of pinging we do to the target url.  Pinging seems
    # to confuse the code-server connection and it loses connection every 20
    # seconds, so for now we'll default to no pinging.
    PING_INTERVAL = None

    # This is the maximum size of frames going to/from the target url. We have
    # seen some frames larger than 3MiB being sent between the jupyter lab client and server.
    MAX_SIZE = 10485760  # 10 MiB

    # These headers are passed through from the client to the target url.
    PASSTHROUGH_HEADERS = {
            'Cookie',
            'User-Agent',
            'Sec-WebSocket-Key',
    }

    @property
    def request_headers(self):
        return {
                h.decode('utf-8').title(): v.decode('utf-8')
                for h, v in self.scope['headers']
        }

    @property
    def headers(self):
        return {
                h: v
                for h, v in self.request_headers.items()
                if h in self.PASSTHROUGH_HEADERS
        }

    @property
    def query_string(self):
        """ The query string initially sent """
        return self.scope['query_string'].decode('utf-8')

    @property
    def target_url(self):
        """ This method must be overriden """
        return None

    @property
    def origin(self):
        return self.request_headers.get('Origin'),

    async def connect(self):
        """Establish connections to both the client and the target url."""

        logger.debug(f'WsProxy for {self.target_url}')

        # The requested url is not valid.
        if self.target_url is None:
            logger.warning('Denying websocket connection.')
            raise DenyConnection('The requested endpoint is not valid.')

        try:
            self.websocket = await websockets.connect(
                    self.target_url,
                    max_size=self.MAX_SIZE,
                    ping_interval=self.PING_INTERVAL,
                    extra_headers=self.headers,
                    subprotocols=self.scope['subprotocols'],
                    origin=self.origin
            )
        except websockets.InvalidURI:
            logger.exception('The requested endpoint could not be reached.')
            raise DenyConnection('The requested endpoint could not be reached.')
        except websockets.InvalidHandshake:
            logger.exception('Communication with the target url was incoherent.')
            raise DenyConnection('Communication with the target url was incoherent.')

        # Accept the client connection. Use the subprotocol negotiated with the
        # target url.
        await self.accept(self.websocket.subprotocol)

        # Forward packets from the target websocket back to the client.
        self.consumer_task = asyncio.create_task(self.consume_from_target())

    async def disconnect(self, close_code):
        """The websocket consumer is shutting down. Shut down the connection to
        the target url."""

        # Disconnect can be called before self.consumer_task is created.

        if hasattr(self, 'consumer_task'):
            self.consumer_task.cancel()

            # Let the task complete
            await self.consumer_task

    async def receive(self, text_data=None, bytes_data=None):
        """Forward packets from the client to the target url."""

        try:
            await self.websocket.send(bytes_data or text_data)
        except websockets.exceptions.ConnectionClosedError:
            # The target probably closed the connection.
            logger.exception('The outgoing connection was closed by the target.')
            await self.close()

    async def consume_from_target(self):
        """A websocket consumer to forward data from the target url to the client."""

        try:
            async for data in self.websocket:
                if hasattr(data, 'decode'):
                    await self.send(bytes_data=data)
                else:
                    await self.send(text_data=data)
        except asyncio.exceptions.CancelledError:
            # This is triggered by the consumer itself when the client connection is terminating.
            logger.debug('Shutting down the websocket consumer task and closing the outgoing websocket.')
            await self.websocket.close()
        except websockets.exceptions.ConnectionClosedError:
            # The target probably closed the connection.
            logger.exception('The outgoing connection was closed by the target.')
            await self.close()
