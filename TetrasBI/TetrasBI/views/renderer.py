# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _

import logging

from dashboards.models.dashboard import Dashboard, Token
from TetrasBI.models.upstream import UpstreamFactory
from TetrasBI.models.renderer import Gate

logger = logging.getLogger(__name__)


def _enter_renderer(request, id, fromToken=False):
    """Enter renderer proxy (nbviewer or voila)
    Store a variable in session to allow proxifying for voila
    even if we are a guest then proxify the request to voila
    """
    if fromToken:
        try:
            token = Token.objects.get(token=id)
        except Token.DoesNotExist:
            raise Http404(_("Token expired"))
        dash = token.dashboard
        sharer = token.user
    else:
        try:
            dash = Dashboard.objects.get(id=id)
            if not request.user.has_perm('dashboards.view_dashboard', dash):
                raise PermissionDenied(_('Not allowed'))
        except Dashboard.DoesNotExist:
            raise Http404(_("Dashboard not found"))
        sharer = None

    renderer = 'nbviewer' if dash.precomputed else 'voila'
    url_params = request.GET.copy()
    path = UpstreamFactory.getUpstream(renderer).getProxifiedPath(url_params, dash.path)
    Gate.allow(request, dash, renderer)
    context = {
        'path': path,
        'title': dash.title,
        'renderer': renderer,
        'sourcesVisible': dash.sourcesVisible,
        'sharer': sharer
    }
    return render(request, 'dashboards/rendered.html', context)


def publicDashboard(request, token):
    """Acces the dashboard from public token if valid
    raise Http404
    """
    return _enter_renderer(request, token, True)


@login_required
def renderDashboard(request, id):
    """Acces the dashboard from id"""
    return _enter_renderer(request, id)
