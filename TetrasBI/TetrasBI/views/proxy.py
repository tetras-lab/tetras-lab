# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt
from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.utils.translation import gettext_lazy as _
from django.utils.http import urlencode
from asgiref.sync import sync_to_async

import re
import logging

from djproxy.views import HttpProxy as BaseHttpProxy
from TetrasBI.views.BaseWebsocketProxy import BaseWebsocketProxy
from six.moves.urllib.parse import urljoin
from TetrasBI.models.upstream import UpstreamFactory

logger = logging.getLogger(__name__)


class AddHeaders(object):
    """ A middleware to add arguments and headers from proxyconfig
    """
    def process_request(self, proxy, request, **kwargs):
        # Add url parameters from proxy config
        args = proxy.getArgs(request)
        if args != {}:
            more_params = urlencode(args).strip('=')
            if kwargs.get('params', None) is not None:
                kwargs['params'] += f'&{more_params}'
            else:
                kwargs['params'] = more_params
        # Add headers from proxy config
        for k, v in proxy.getHeaders().items():
            kwargs['headers'][k] = v
        # Add Host headers for wss to work
        kwargs['headers']['HOST'] = settings.EXTERNAL_HOST
        return kwargs


class appProxy(object):
    """ A middleware to proxify voilà/jupyter apps like dash
    """
    def shouldActivate(self, proxy, url):
        return proxy.hasAppProxy() and f'{proxy.getTo()}/app_proxy' in url

    def process_request(self, proxy, request, **kwargs):
        to = proxy.getTo()
        if self.shouldActivate(proxy, kwargs['url']):
            base = proxy.getBaseUrlWithoutPort()
            old_url = kwargs['url']
            kwargs['url'] = re.sub(fr'({base}):\d*/{to}/app_proxy/([\d]*/?.*)', r'\1:\2', kwargs['url'])
        return kwargs


class ProxyWithUpstreamMixin():
    def init_upstream(self, name, proto, url):
        logger.debug(f'Initializing {proto}Proxy for {name} at {url}')
        self._tlupstream = UpstreamFactory.getUpstream(name)
        self.proto = proto
        self._kwargs_url = url.strip('/')
        if (self._tlupstream is None):
            raise Http404(_('No such proxy'))
        self._base_url_without_port = f'{self.proto}://{self._tlupstream.host}'
        self.base_url = f'{self._base_url_without_port}:{self._tlupstream.port}'

    def check_perms(self, request):
        if (not self._tlupstream.allowed(request)):
            raise PermissionDenied(_('Not allowed'))

    def getArgs(self, request):
        return self._tlupstream.getArgs(request)

    def getHeaders(self):
        return self._tlupstream.headers

    def getTo(self):
        return self._tlupstream.name

    def hasAppProxy(self):
        return self._tlupstream.appProxyEnabled

    def getBaseUrlWithoutPort(self):
        return self._base_url_without_port

    @property
    def proxy_url(self):
        path = self._tlupstream.path.strip('/') + '/' + self._kwargs_url
        ret = urljoin(self.base_url, path)
        logger.debug(f'Proxying to {ret}')
        return ret


@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(xframe_options_exempt, name='dispatch')
class HttpProxy(ProxyWithUpstreamMixin, BaseHttpProxy):
    timeout = (3, settings.REQUEST_TIMEOUT)
    _tlupstream = None
    proxy_middleware = [
        'TetrasBI.views.proxy.appProxy',
        'djproxy.proxy_middleware.AddXFF',
        'djproxy.proxy_middleware.AddXFH',
        'djproxy.proxy_middleware.AddXFP',
        'TetrasBI.views.proxy.AddHeaders',
        'djproxy.proxy_middleware.ProxyPassReverse',
    ]

    def setup(self, request, **kwargs):
        # Retrieve proxy settings
        self.init_upstream(kwargs['proxy'], 'http', kwargs.get('url', ''))
        self.check_perms(request)
        self.reverse_urls = [
            (f'{self.getTo()}', self.base_url.strip('/'))
        ]
        super().setup(request, kwargs)


class WSRequest():
    """ A simple class to emulate django request in WebsocketProxy """
    def __init__(self, scope):
        self.scope = scope
        self.user = self.scope['user']
        self.session = self.scope['session']

    def get_full_path(self):
        return self.scope['path']


class WebsocketProxy(BaseWebsocketProxy, ProxyWithUpstreamMixin):
    """ A websocket proxy specific for ou usage """
    @property
    def headers(self):
        return dict(
            # Headers from BaseWebsocketProxy
            super().headers,
            # Headers from ProxyWithUpstreamMixin
            **self.getHeaders(),
            # Add XFF, XFH and XFP
            **{
                'X-Forwarded-For': self.scope['client'][0],
                'X-Forwarded-Host': settings.EXTERNAL_HOST,
                'X-Forwarded-Proto': settings.PROTO
            })

    @property
    def target_url(self):
        return f'{self.proxy_url}?{self.query_string}'

    @property
    def origin(self):
        return settings.EXTERNAL_HOST

    @sync_to_async
    def check_perms(self):
        return super().check_perms(self.request)

    async def connect(self):
        self.request = WSRequest(self.scope)
        self.init_upstream(self.scope["url_route"]["kwargs"]["proxy"], 'ws', self.scope['url_route']['kwargs']['url'])
        await self.check_perms()
        await super().connect()
