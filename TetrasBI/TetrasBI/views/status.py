# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Poujade, Loïs <lpo@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic.base import View
from django.conf import settings
from django.shortcuts import render, redirect
from django.utils.translation import gettext as _
from django.http import JsonResponse


from ..models.admin import Admin

import logging

logger = logging.getLogger(__name__)


class StatusView(View):
    def get_context_data(self, *args, **kwargs):
        adm_model = Admin()
        status = {}
        (status['errors'], status['info']) = adm_model.get_info()
        if len(status['errors']) > 0:
            return 'error'
        broken_services = [x for x in status['info']['services'] if status['info']['services'][x]['status'] is not True]
        status = 'up' if len(broken_services) == 0 else 'down'
        return status

    def get(self, request, *args, **kwargs):
        return JsonResponse({'status': self.get_context_data()})
