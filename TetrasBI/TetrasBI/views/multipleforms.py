# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.views.generic.edit import FormView
from django.forms.models import ModelForm
from django.utils.translation import gettext_lazy as _
from django.contrib import messages


class MultipleFormsView(FormView):
    template_name = ''
    form_classes = {}

    def get_initial_form_kwargs(self, key):
        return {}

    def _init_form(self, key, cl, with_post):
        form_kwargs = self.get_initial_form_kwargs(key)
        form_kwargs['prefix'] = key
        if with_post:
            form_kwargs['files'] = self.request.FILES
            form_kwargs['data'] = self.request.POST
        return cl(**form_kwargs)


    def get_context_data(self, *args, **kwargs):
        return {'forms': {k: self._init_form(k, cl, False) for k, cl in self.form_classes.items()}}

    def is_form_submited(self, key, form):
        for f in form.fields:
            if f'{form.prefix}-{f}' in self.request.POST:
                return True
        return False

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(*args, **kwargs)
        for k, cl in self.form_classes.items():
            form = self._init_form(k, cl, True)
            if self.is_form_submited(k, form):
                context['forms'][k] = form
                if form.is_valid():
                    self.form_valid(k, form)
        return self.render_to_response(context)
