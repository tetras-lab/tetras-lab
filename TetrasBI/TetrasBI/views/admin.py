# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Poujade, Loïs <lpo@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.contrib import messages
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from django.utils.translation import gettext as _

from rules.contrib.views import PermissionRequiredMixin

from ..forms.admin import ThemeEditForm
from ..models.admin import Admin
from ..models.theme import Theme

import logging

logger = logging.getLogger(__name__)


class AdminView(PermissionRequiredMixin, FormView):
    permission_required = 'TetrasBI.administrate_platform'
    template_name = 'admin/index.html'
    form_class = ThemeEditForm
    success_url = '.'
    _theme_object = None

    @property
    def theme_object(self):
        if self._theme_object is None:
            requested_files = {
                'files': self.request.FILES,
                'fields': self.request.POST.items()
            }
            form_labels = {
                'color_fields': self.form_class.color_fields,
                'image_fields': self.form_class.image_fields
            }
            self._theme_object = Theme(requested_files, form_labels)
        return self._theme_object

    def get_context_data(self, *args, **kwargs):
        adm_model = Admin()
        context = super().get_context_data(*args, **kwargs)
        (errors, context['info']) = adm_model.get_info()
        for e in errors:
            messages.add_message(self.request, messages.ERROR, e)
        return context

    def get_initial(self):
        initial = super().get_initial()
        for field in self.form_class.color_fields:
            initial[field] = Theme.get_css_value(field)
        return initial

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super().get_form_kwargs(**kwargs)
        for key in self.form_class.image_fields:
            form_kwargs[key + '_image_url'] = Theme.get_image(key)['url']
            form_kwargs[key + '_image_path'] = Theme.get_image(key)['path']
            form_kwargs[key + '_image_is_clearable'] = False if 'default' in form_kwargs[key + '_image_url'] else True
        # Special loading logo case
        if form_kwargs['loading_image_url'] == form_kwargs['logo_image_url']:
            form_kwargs['loading_image_is_clearable'] = False
        return form_kwargs

    def form_valid(self, form):
        user = self.request.user
        if not (user.has_perm('TetrasBI.administrate_platform')):
            raise PermissionDenied(_('Not allowed'))
        extensions = {key: "."+value['extension'] for key, value in self.form_class.images_parameters.items()}
        filesystem_storage = FileSystemStorage(location=settings.THEME_ROOT)
        self.theme_object.save_new_files(extensions, filesystem_storage)
        self.theme_object.delete_files(extensions, filesystem_storage)
        self.theme_object.write_css(form)
        Theme.update_assets()
        messages.add_message(self.request, messages.SUCCESS, _('Theme successfully updated'))
        messages.add_message(self.request, messages.SUCCESS, _('If you don\'t see any changes, please clear the browser cache'))
        return super().form_valid(form)

    def form_invalid(self, form):
        context = self.get_context_data(form=form)
        context["theme_post"] = True
        return render(self.request, AdminView.template_name, context)


class AdminThemeResetView(PermissionRequiredMixin, View):
    permission_required = 'TetrasBI.administrate_platform'

    def post(self, request, *args, **kwargs):
        user = self.request.user
        if not (user.has_perm('TetrasBI.administrate_platform')):
            raise PermissionDenied(_('Not allowed'))
        # Delete files
        filesystem_storage = FileSystemStorage(location=settings.THEME_ROOT)
        for filename in filesystem_storage.listdir(settings.THEME_ROOT)[1]:
            Theme.safe_delete(settings.THEME_ROOT / filename, filesystem_storage)
        # Initialize theme.scss
        with open(settings.THEME_ROOT / 'theme.scss', 'w', encoding='UTF-8') as file:
            file.write("")
        # Update assets
        Theme.update_assets()
        messages.add_message(self.request, messages.SUCCESS, _('Theme successfully reset'))
        return redirect('admin')
