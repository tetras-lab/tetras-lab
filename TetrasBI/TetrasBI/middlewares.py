# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" A simple middleware to intercept and handle exceptions
"""

from django.contrib import messages
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import Http404, FileResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.utils.translation import activate as set_lang

import re
from pathlib import Path


class FirstRun:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not User.objects.exists() and 'firstrun' not in request.path:
            return redirect(reverse_lazy('first-run'))
        return self.get_response(request)


class SetLang:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user
        if hasattr(user, 'lang') and user.lang.lang:
            set_lang(user.lang.lang)
        return self.get_response(request)


class MediaMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def get_media(self, path):
        path = Path(f'{settings.MEDIA_ROOT}/{path}').resolve()
        if path.is_dir():
            raise PermissionDenied(_('Accessing directories is not allowed'))
        if not path.exists():
            raise Http404(_('File "%(path)s" not found"') % {'path': path})
        return FileResponse(open(path.as_posix(), 'rb'), as_attachment=False, filename=path.name)

    def __call__(self, request):
        groups = re.match('/' + settings.MEDIA_URL.strip("/") + '/(.*)', request.path)
        path = request.path
        url = settings.MEDIA_URL.strip("/")
        regex = settings.MEDIA_URL.strip("/") + r'/(.*)$'
        if groups is not None:
            return self.get_media(groups[1])
        return self.get_response(request)


class ExceptionHandler:
    handled = (PermissionDenied, Http404)

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """ Do nothing here
        """
        return self.get_response(request)

    def process_exception(self, request, exception):
        if self._should_process(request, exception):
            if exception is not None:
                for a in exception.args:
                    messages.error(request, a)
            if isinstance(exception, PermissionDenied) and not request.user.is_authenticated:
                return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
            return redirect('/')

    def _is_ajax(self, request):
        return request.headers.get('x-requested-with') == 'XMLHttpRequest'

    def _should_process(self, request, exception):
        return not self._is_ajax(request) and (settings.PROD or isinstance(exception, self.handled))
