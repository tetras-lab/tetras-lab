# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from abc import ABC

from dashboards.models.dashboard import Dashboard, Token


class Gate(ABC):
    @staticmethod
    def allow(request, dash, renderer):
        """Allow rendering the given dashboard with the renderer"""
        request.session['dashid'] = dash.id
        request.session[renderer] = dash.path

    @staticmethod
    def is_allowed(request, name, forbidden_regex, required_path):
        """Checks that the request has previously been allowed"""
        user = request.user
        path = request.get_full_path()
        allowed_path = request.session.get(name, None)
        dashid = request.session.get('dashid', None)
        if allowed_path is None or dashid is None:
            return False
        # Make sure that the user is allowed to render this dashboard
        try:
            dash = Dashboard.objects.get(id=dashid)
        except Dashboard.DoesNotExist:
            raise Http404(_("Dashboard not found"))
        if not user.has_perm('dashboards.render_dashboard', (dash, request)):
            return False

        # Last check : verify that the requested path is allowed
        return (not bool(re.match(forbidden_regex, path))
                and (required_path not in path or f'/{allowed_path}' in path))
