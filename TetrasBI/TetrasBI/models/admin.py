# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Poujade, Loïs <lpo@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import yaml
from os import environ
from subprocess import check_output, call
from django.utils.translation import gettext as _

logger = logging.getLogger(__name__)


class Admin():
    install_root = '/app/'
    errors = []

    def get_info(self):
        """
        Gather current install info (version, commits etc) and return them as dict.
        Each section got a list of values like the following:
        { key: {status: bool, data: '...', additional: '...'} }
        `key` and `data` is always shown, `additionnal` only if present, and `status` is used only if not none
        if `data` key is not found, content for `key` is used as only data
        """
        self.errors = []
        info = {
                'services': self.containers_info(),
                'git': self.git_info(),
                'environ': self.env_info(),
                'python': self.pip_info()
            }
        return (self.errors, info)

    def env_info(self):
        """Read some vars from environ"""
        info = {}
        for var in ['FLAVORS', 'FLAVOR_SCRIPT', 'COMPOSE_FILE']:
            if not var in environ:
                logger.warn(f"missing env var '{var}'")
                info[f'${var}'] = {'status': False, 'data': f"missing {var}"}
            else:
                info[f'${var}'] = {'status': None, 'data': environ[var]}

        if '$FLAVOR_SCRIPT' in info.keys():
            try:
                with open(environ['FLAVOR_SCRIPT'], 'rt') as fl_script:
                    basedata = environ['FLAVOR_SCRIPT']
                    info['FLAVOR_SCRIPT'] = {
                            'status': None, 'data': basedata,
                            'additionnal': ''.join(fl_script.readlines()),
                            'additionnal_title': environ['FLAVOR_SCRIPT']
                            }
            except Exception as e:
                logger.warn(f"can't read flavor script at {environ['FLAVOR_SCRIPT']}: {e}")
                info['FLAVOR_SCRIPT'] = {
                        'status': False,
                        'data': f"failed to read flavor script at '{environ['FLAVOR_SCRIPT']}': {e}"
                        }
        return info


    def git_info(self):
        """Get git repo status/informations"""
        info = {}
        try:
            git_cmd = ['git', '-C', Admin.install_root]
            git_status = check_output([*git_cmd, 'status'], text=True)
            git_status += check_output([*git_cmd, 'diff', '--stat'], text=True)
            repo_status = call([*git_cmd, 'diff', '--quiet'])
            info['git_repo_status'] = {
                    'status': repo_status == 0,
                    'data': 'clean' if repo_status == 0 else 'dirty',
                    'additionnal': git_status,
                    'additionnal_title': 'git status ; git diff --stat'
            }
            info['git_branch'] = {'status': None, 'data': check_output([*git_cmd, 'branch', '--show-current'], text=True)}
            info['git_last_commit'] ={'status': None, 'data':  check_output([*git_cmd, 'log', '-n1', "--format='%h %an %ai %s'"], text=True)}
        except Exception as e:
            msg = f"[git] failed to run `git status; git diff --stat`: {e}"
            logger.warn(msg, e)
            self.errors.append(msg)
        return info


    def pip_info(self):
        """Get currently installed pip packages & their versions"""
        info = {}
        try:
            container_pip_freeze = check_output(['python', '-m', 'pip', 'freeze'], text=True)
            for item in container_pip_freeze.splitlines():
                (package, version) = item.split('==')
                info[package] = {'status': None, 'data': version}
        except Exception as e:
            msg = f"[python] failed to run `python -m pip freeze`: {e}"
            logger.warn(msg, e)
            self.errors.append(msg)
        return info

    def get_services(self, path):
     with open(path) as f:
         d = yaml.load(f, Loader=yaml.FullLoader)
     return [k for k in d['services'].keys()] if 'services' in d else []

    def containers_info(self):
        """Try to ping containers"""
        containers = {}
        services = []
        for yml_file in environ['COMPOSE_FILE'].split(':'):
            try:
                services += self.get_services(self.install_root + yml_file)
            except Exception as e:
                msg = f"[containers] failed to read {yml_file}: {e}"
                logger.warn(msg, e)
                self.errors.append(msg)


        for container in services:
            try:
                result = call(['/bin/ping', '-c1', container])
            except Exception as e:
                msg = f"[containers] failed to run `ping -c1 {container}`: {e}"
                logger.warn(msg, e)
                self.errors.append(msg)
                result = 1
            if result != 0:
                message = _('unreachable')
            else:
                message = _('up')
            containers[container] = {'status': result == 0, 'data': message}

        return containers




