import subprocess

from django.contrib.staticfiles import finders
from django.contrib.staticfiles.utils import get_files
from django.contrib.staticfiles.storage import StaticFilesStorage

from django.conf import settings
from django.core import management


class Theme:

    def __init__(self, requested_files, form_labels):
        self.requested_files = requested_files
        self.form_labels = form_labels

    def save_new_files(self, extensions, filesystem_storage):
        for filename, file in self.requested_files['files'].items():
            if filename in self.form_labels['image_fields']:
                full_path = Theme._full_path(filename, extensions)
                file = self.requested_files['files'][filename]
                Theme.safe_delete(full_path, filesystem_storage)
                filesystem_storage.save(full_path, file)

    def delete_files(self, extensions, filesystem_storage):
        for field_name, value in self.requested_files['fields']:
            filename = field_name.split('-clear')[0]
            if filename in self.form_labels['image_fields'] and value == 'on':
                full_path = Theme._full_path(filename, extensions)
                Theme.safe_delete(full_path, filesystem_storage)

    def write_css(self, form):
        css_string = "\n".join(
            ["$" + color + ": " + form.cleaned_data[color] + ";" for color in self.form_labels['color_fields']]
        )
        with open(settings.THEME_ROOT / 'theme.scss', 'w', encoding='UTF-8') as file:
            file.write(css_string)

    @staticmethod
    def _full_path(filename, extensions):
        filename_with_extension = filename + extensions[filename]
        return settings.THEME_ROOT / filename_with_extension

    @staticmethod
    def get_css_value(key):
        line = Theme._get_css_line_from_file('custom', key)
        if len(line) == 0:
            line = Theme._get_css_line_from_file('default', key)
        return line[-1].split(':')[1].strip()[:-1].split(';')[0]

    @staticmethod
    def _get_css_line_from_file(directory, key):
        with open(settings.STATIC_ROOT + 'theme/' + directory + '/theme.scss', 'r', encoding='UTF-8') as file:
            lines = [line.strip() for line in file]
        return [l for l in lines if l.startswith('$' + str(key))]

    @staticmethod
    def get_image(key):
        s = StaticFilesStorage()
        ignore_patterns = ["*.*.*"]
        location = 'theme/custom/'
        filepaths = Theme._get_filepaths(s, ignore_patterns, location, key)
        if len(filepaths) == 0:
            location = 'theme/default/'
            filepaths = Theme._get_filepaths(s, ignore_patterns, location, key)
        filepath = filepaths[0]
        return {'url': settings.STATIC_URL + filepath, 'path': settings.STATIC_ROOT + filepath}

    @staticmethod
    def _get_filepaths(s, ignore_patterns, location, key):
        file_list = list(get_files(s, ignore_patterns, location=location))
        filepaths = [f for f in file_list if f.startswith(location + key) and finders.find(f)]
        # Default image for optional loading logo
        if key == 'loading' and len(filepaths) == 0:
            logo_filepaths = [f for f in file_list if f.startswith(location + 'logo') and finders.find(f)]
            if len(logo_filepaths) > 0:
                filepaths = logo_filepaths
        return filepaths

    @staticmethod
    def safe_delete(full_path, filesystem_storage):
        if filesystem_storage.exists(full_path):
            filesystem_storage.delete(full_path)

    @staticmethod
    def update_assets():
        subprocess.call("npx webpack", shell=True)
        management.call_command("collectstatic", "--no-input")
        management.call_command("clear_cache")
