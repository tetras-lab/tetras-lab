# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import environ
import sys
import requests
import json
import urllib.parse

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.staticfiles import finders
from django.templatetags.static import static
from django.utils.translation import gettext as _

from TetrasBI.models.renderer import Gate
from TetrasBI.models.theme import Theme

from pathlib import Path
from urllib.parse import urljoin


class UpstreamFactory():
    # Classes that should NOT be instanciated by the factory
    FORBIDDEN = ['Upstream']

    @classmethod
    def getUpstream(cls, name):
        # If name is voila classname should be Voila
        classname = f'{name.capitalize()}'
        if classname in cls.FORBIDDEN:
            return None
        # Get current module object
        module = sys.modules[__name__]
        try:
            return getattr(module, classname)(name)
        except AttributeError:
            return None


class Upstream():
    # host port and path will be overided by XXX_YYY environement variables if defined
    # Were XXX self.name.upper() and YYY is PORT|HOST|PATH
    # Upstream host
    host = 'localhost'
    # Upstream port
    port = 80
    # Upstream base path
    path = ''
    # Aguments to add in request for upstream
    args = {}
    # headers to add in request for upstream
    headers = {}
    # If the proxy is a renderer this is the base path of a rendered url
    render_url = None
    # Urls that are not starting with this path are forbidden in renderers
    required_path = None
    # Regex path that should be forbidden for renderer
    forbidden_regex = None
    # Do we enable urls like myproxy/app_proxy/port/some/path to be proxified to myproxy:port/some/path
    appProxyEnabled = False

    def __init__(self, name):
        self.name = name
        cap = name.upper()
        self.host = environ.get(f'{cap}_HOST', self.host)
        self.port = environ.get(f'{cap}_PORT', self.port)
        self.path = environ.get(f'{cap}_PATH', self.path)

    def getProxifiedPath(self, url_params, path):
        """ Return the proxified path for a given url and params
        @param url_params:  string url paremeters (not urlencoded)
        @param path:        string path on the upstream
        @return             string
        """
        url_params.update(self.args)
        return f'{self.path}{self.render_url}{path}?{url_params.urlencode()}'

    def apiCall(self, action, obj, payload={}, scheme="http", args={}):
        """ Returns the result of a call to an api.
        @param action: string, one of 'create', 'read', 'update' or 'delete'.
        @param obj: the object to be called via the api.
        @param payload: dict with one or more among 'id'.
        @param scheme: 'http' or 'https'.
        @param args
        """
        raise NotImplementedError

    def allowed(self, request):
        """Checks that the request is allowed to be forwarded upstream
        @param request: the request
        @return             Boolean
        """
        return (request.user.has_perm(f'TetrasBI.access_{self.name}') and
                (self.render_url is None or
                 Gate.is_allowed(request, self.name, self.forbidden_regex, self.required_path)
                 ))

    def getArgs(self, request):
        return self.args

    def getUpstreamUrlWithQueryString(self, user):
        """Returns an url with possible queryString"""
        raise NotImplementedError


class Voila(Upstream):
    port = 8866
    render_url = 'voila/render/'
    required_path = '/render/'
    forbidden_regex = '^/voila/?$'
    appProxyEnabled = True


class Jupyter(Upstream):
    port = 8888
    appProxyEnabled = True

    def __init__(self, name):
        super().__init__(name)
        self.headers = self._getHeaders()

    def _getHeaders(self):
        return {'Authorization': 'token ' + environ.get('JUPYTER_TOKEN', '')}

    def getArgs(self, request):
        user = request.user
        return self.getParams(user)

    def getParams(self, u):
        username = u.username
        name = ''
        initials = ''
        color = self.getTheme()
        url = 'theme/custom/logo.png'
        if finders.find(url):
            avatar_url = static(url)
        else:
            avatar_url = static(url.replace('custom', 'default'))
        if u.first_name:
            name = u.first_name.capitalize()
            initials = u.first_name[0].upper()
        if u.last_name:
            if name:
                name += ' '
            name += u.last_name.capitalize()
            initials += u.last_name[0].upper()
        if not (u.first_name or u.last_name):
            name = username.capitalize()
            initials = username[:2].upper()
        display_name = name
        admin = u.role.admin
        user = {'username': username, 'name': name, 'display_name': display_name, 'initials': initials,
                'avatar_url': avatar_url, 'color': color, 'admin': admin}
        return {
            'user': urllib.parse.urlencode(user),
            'reset': ''
        }

    def getTheme(self):
        try:
            color = Theme.get_css_value('primaryBg')
        except:
            color = 'transparent'
        return color

    def apiCall(self, action, obj, payload={}, scheme="http", args={}):
        dashboards_filesystem_root = Path(settings.DASHBOARD_PATH).name
        base_url = f"{scheme}://{self.host}:{self.port}"
        scheduler_path = Path(f"{self.path}scheduler").resolve()
        files_path = Path(f"{self.path}files").resolve()
        dashboard_api_path = Path(f"{self.path}api").resolve() / "contents" / dashboards_filesystem_root

        # All scenarios depend on `action` and `obj`
        if obj == "workspace":
            user = User.objects.get(username=args["username"])
            workspace_path = Path(f"{self.path}lab").resolve() / "api" / "workspaces" / args["username"]
            url_workspace = urljoin(base_url, str(workspace_path))
            if action == "delete":
                response = requests.delete(url_workspace, headers=self._getHeaders(), params=self.getParams(user))
            elif action == "put":
                response = requests.put(url_workspace, headers=self._getHeaders(), data=json.dumps(payload))
            else:
                raise NotImplementedError
            if response.status_code != 204:
                response = response.json()
            else:
                response = {}
            return response

        if action == "create" and obj == "jobs":
            # One-off job creation from an existing job definition (`run now` button)
            # Special case: the url is linked to a job definition, but it's a job that's being created
            create_jobs_path = scheduler_path / "job_definitions" / payload['job_definition_id'] / "jobs"
            url_scheduler = urljoin(base_url, str(create_jobs_path))
            return requests.post(url_scheduler, data=json.dumps(payload), headers=self._getHeaders()).json()
        else:
            # General case: the url ends with the processed object, and possibly, an id
            scheduler_path = scheduler_path / f"{obj}"
            if payload.get("id", ""):
                scheduler_path = scheduler_path / f"{payload['id']}"
            url_scheduler = urljoin(base_url, str(scheduler_path))

        if action == "create" and obj == "job_definitions":
            # Job definition creation
            return requests.post(url_scheduler, data=json.dumps(payload), headers=self._getHeaders()).json()
        elif action == "read" and obj in ["jobs", "job_definitions"]:
            # Job or job definition reading
            return requests.get(url_scheduler, headers=self._getHeaders()).json()
        elif action == "update" and obj in ["jobs", "job_definitions"]:
            # Job or job definition update
            if payload.get("input_uri", ""):
                # As of today's date (July 2023), jupyter scheduler cannot update the snapshot with the same file name.
                # There is a workaround. We create an empty file and update it in two steps:
                # 1. Initial file is replaced with empty file. 2. Empty file is replaced with updated file.
                input_uri = payload["input_uri"]
                try:
                    # Notebook creation
                    creation_payload = {"type": "notebook", "path": dashboards_filesystem_root}
                    url_dashboard_api = urljoin(base_url, str(dashboard_api_path))
                    response = requests.post(url_dashboard_api, data=json.dumps(creation_payload), headers=self._getHeaders())
                    empty_file_name = json.loads(response.content).get('name')
                    # Replace initial notebook with empty file
                    payload["input_uri"] = str(Path(dashboards_filesystem_root) / empty_file_name)
                    requests.patch(url_scheduler, data=json.dumps(payload), headers=self._getHeaders())
                    # Delete empty file
                    dashboard_api_path = dashboard_api_path / empty_file_name
                    url_dashboard_api = urljoin(base_url, str(dashboard_api_path))
                    requests.delete(url_dashboard_api, headers=self._getHeaders())
                    payload["input_uri"] = input_uri
                except Exception as e:
                    raise
            # Replace file with an updated one (or a new one)
            response = requests.patch(url_scheduler, data=json.dumps(payload), headers=self._getHeaders())
        elif action == "delete" and obj in ["jobs", "job_definitions"]:
            # job or job definition deletion
            response = requests.delete(url_scheduler, headers=self._getHeaders())
        elif action == "export_html" and obj == "jobs":
            # job output export to html file
            scheduler_path = scheduler_path / "download_files"
            url_scheduler = urljoin(base_url, str(scheduler_path))
            response = requests.get(url_scheduler, headers=self._getHeaders(), params={"redownload": "false"})
        elif action == "get_html" and obj == "jobs":
            # job html output reading
            files_path = files_path / payload['path']
            url_files = urljoin(base_url, str(files_path))
            return requests.get(url_files, headers=self._getHeaders()).text
        else:
            raise NotImplementedError(_("The following API call is not implemented: ") + str(action))
        if response.status_code != 204:
            response = response.json()
        else:
            response = {}
        return response

    def getUpstreamUrlWithQueryString(self, user):
        params = {'theme': self.getTheme()}
        return urllib.parse.urlunsplit(
            ['', '', str(Path('/jupyter/lab/workspaces/').resolve() / user.username),  urllib.parse.urlencode(params), '']
        )


class Nbviewer(Upstream):
    port = 5000
    args = {'flush_cache': 'true'}
    render_url = 'localfile/dashboards/'
    required_path = '/localfile/'
    forbidden_regex = '^/nbviewer(/localfile|)/?$'


class Webvowl(Upstream):
    port = 8080
