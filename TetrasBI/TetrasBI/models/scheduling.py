from django.conf import settings

from django.utils.translation import gettext as _

from datetime import datetime
import ast
import pytz

from cron_descriptor import Options, ExpressionDescriptor
from pathlib import Path

from TetrasBI.models.upstream import UpstreamFactory


class Scheduling:

    def __init__(self, job_defs, jobs):
        self.job_defs = job_defs
        self._job_uri_to_job_filename()
        self.jobs = jobs
        self.append_jobs_to_job_defs()
        self.expose_last_completed_and_failed_job()
        self.timezone = self._get_timezone()

    def _job_uri_to_job_filename(self):
        if 'job_definitions' in self.job_defs:
            for index, job_def in enumerate(self.job_defs['job_definitions']):
                self.job_defs['job_definitions'][index]['input_uri'] = self.job_defs['job_definitions'][index]['input_filename']

    def append_jobs_to_job_defs(self):
        if 'jobs' in self.jobs:
            for job in self.jobs['jobs']:
                job_def_id = job.get('job_definition_id', '')
                if job_def_id:
                    self._append_job_to_job_def(job, job_def_id)
            self._sort_jobs_by_start_time()

    def _append_job_to_job_def(self, job, job_def_id):
        for index, job_def in enumerate(self.job_defs['job_definitions']):
            if job_def['job_definition_id'] == job_def_id:
                if not self.job_defs['job_definitions'][index].get('jobs', ''):
                    self.job_defs['job_definitions'][index]['jobs'] = [job]
                else:
                    self.job_defs['job_definitions'][index]['jobs'].append(job)
                break

    def _sort_jobs_by_start_time(self):
        for index, job_def in enumerate(self.job_defs['job_definitions']):
            if not self.job_defs['job_definitions'][index].get('jobs', ''):
                self.job_defs['job_definitions'][index]['jobs'] = []
            self.job_defs['job_definitions'][index]['jobs'].sort(key=lambda x: -x['start_time'])

    def expose_last_completed_and_failed_job(self):
        if 'job_definitions' in self.job_defs:
            for index, job_def in enumerate(self.job_defs['job_definitions']):
                self._set_last_job(index, job_def, 'COMPLETED')
                self._set_last_job(index, job_def, 'FAILED')

    def _set_last_job(self, index, job_def, status):
        if status == 'COMPLETED':
            key = 'last_completed_job'
        elif status == 'FAILED':
            key = 'last_failed_job'
        else:
            raise ValueError(f"Status {status} is not valid.")
        self.job_defs['job_definitions'][index][key] = ""
        for job in job_def['jobs']:
            if job['status'] == status:
                self.job_defs['job_definitions'][index][key] = job
                break

    def make_time_and_cron_human_readable(self, lang):
        options = Options()
        options.use_24hour_time_format = True
        week_day = {
            'Monday': _('Monday'), 'Tuesday': _('Tuesday'), 'Wednesday': _('Wednesday'), 'Thursday': _('Thursday'),
            'Friday': _('Friday'), 'Saturday': _('Saturday'), 'Sunday': _('Sunday')
        }
        options.locale_code = settings.LOCALES_CODE[lang].replace('-', '_')
        if 'job_definitions' in self.job_defs:
            for index, job_def in enumerate(self.job_defs['job_definitions']):
                # Human-readable schedule
                try:
                    self.job_defs['job_definitions'][index]['schedule_human_readable'] = \
                        ExpressionDescriptor(job_def['schedule'], options).get_description()
                    for d in week_day:
                        self.job_defs['job_definitions'][index]['schedule_human_readable'] = \
                            self.job_defs['job_definitions'][index]['schedule_human_readable'].replace(d, week_day.get(d, d))
                except Exception:
                    self.job_defs['job_definitions'][index]['schedule_human_readable'] = job_def['schedule']
                # Human-readable date
                self.job_defs['job_definitions'][index]['create_time_human_readable'] = \
                    self._datetime_human_readable(job_def['create_time'])
                self.job_defs['job_definitions'][index]['update_time_human_readable'] = \
                    self._datetime_human_readable(job_def['update_time'])
                for i, job in enumerate(self.job_defs['job_definitions'][index]['jobs']):
                    self.job_defs['job_definitions'][index]['jobs'][i]['start_time_human_readable'] = \
                        self._datetime_human_readable(job['start_time'])
                    if job.get('end_time', ''):
                        self.job_defs['job_definitions'][index]['jobs'][i]['end_time_human_readable'] = \
                            self._datetime_human_readable(job['end_time'])

    def _datetime_human_readable(self, timestamp):
        return datetime.fromtimestamp(timestamp / 1000, pytz.timezone(self.timezone)).strftime('%Y-%m-%d %H:%M:%S')

    def _get_timezone(self):
        timezone = pytz.utc.zone
        if 'job_definitions' in self.job_defs:
            for job_def in self.job_defs['job_definitions']:
                if job_def.get('timezone'):
                    timezone = job_def['timezone']
                    break
        return timezone

    def _apiCall(self, job):
        path = next(jf["file_path"] for jf in job['job_files'] if jf['file_format'] == 'html')
        return UpstreamFactory.getUpstream("jupyter").apiCall("get_html", "jobs", payload={"path": path})

    def get_job_def(self, job_def_id):
        return next(jd for jd in self.job_defs['job_definitions'] if jd['job_definition_id'] == job_def_id)

    def get_job(self, job_id):
        return next(j for jd in self.job_defs['job_definitions'] for j in jd['jobs'] if j['job_id'] == job_id)

    def get_html(self, job_id):
        job = self.get_job(job_id)
        return self._apiCall(job)

    @staticmethod
    def format_job_def_post_data(data):
        # User input parameters are transformed into the format expected by the scheduler API
        # e.g.: a = 8\r\nb = 5 is transformed into {a: 8, b: 5}
        if "parameters" in data:
            if data["parameters"].strip() != "":
                data["parameters"] = {
                    line.split("=")[0].strip(): line.split("=")[1].strip()
                    for line in data["parameters"].split("\r\n")
                }
            else:
                del data["parameters"]
        # User input tags are also transformed. e.g.: firstTag, secondTag is transformed into [firstTag, secondTag]
        if "tags" in data:
            if data["tags"].strip() != "":
                data["tags"] = [t.strip() for t in data["tags"].split(",") if t.strip() != ""]
            else:
                del data["tags"]
        # The input URI of a selected dashboard must be prefixed by the root of dashboards filesystem
        if "input_uri" in data:
            if data["input_uri"]:
                dashboards_filesystem_root = Path(settings.DASHBOARD_PATH).name
                data["input_uri"] = str(Path(dashboards_filesystem_root) / data["input_uri"])
            else:
                del data["input_uri"]
        if "output_formats" in data:
            data["output_formats"] = ast.literal_eval(data["output_formats"])
        return data

    @staticmethod
    def initialize(field, job_def):
        value = job_def.get(field, '')
        if field == 'parameters' and value != '':
            return '\r\n'.join([str(key) + ' = ' + str(val) for key, val in value.items()])
        elif field == 'tags' and value != '':
            return ', '.join(value)
        elif field == 'old_input_uri':
            return job_def.get('input_uri', '')
        elif field == 'input_uri':
            return ''
        else:
            return value
        return ''

