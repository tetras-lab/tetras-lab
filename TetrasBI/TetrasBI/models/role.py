# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.utils.crypto import get_random_string
from string import ascii_lowercase


class Role(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    admin = models.BooleanField(default=False)
    data_scientist = models.BooleanField(default=False)
    shared_account = models.BooleanField(default=False)
    guest = models.BooleanField(default=False)
    ROLES = {
        'admin': _('Administrator'),
        'data_scientist': _('Data Scientist'),
        'shared_account': _('shared account'),
        'guest': _('Guest')
    }

    def hasRole(self, name):
        if name not in self.ROLES:
            raise Exception(_('Unknown role: "{name}"'.format(name=name)))
        return getattr(self, name)

    def getRoles(self):
        if self.user.is_superuser:
            return [_("superuser")]
        roles = []
        for r in self.ROLES:
            if getattr(self, r):
                roles.append(r)
        return roles

    def getUsersBelowMe(self):
        users = User.objects.all()
        my_roles = self.getRoles()
        if 'admin' in my_roles:
            pass
        elif 'data_scientist' in my_roles:
            users = users.exclude(id__in=Role.objects.filter(Q(admin=1) | Q(data_scientist=1)).values_list('user_id', flat=True))
        else:
            users = User.objects.none()
        return users.values_list('id', flat=True)

    @staticmethod
    def create_or_get_guest_user():
        guest_queryset = User.objects.filter(role__guest=1)
        if guest_queryset.count() > 0:
            guest = guest_queryset.first()
        else:
            guest_login = 'guest_' + get_random_string(6, ascii_lowercase)
            guest_password = get_random_string(10, ascii_lowercase)
            guest = User.objects.create_user(username=guest_login, password=guest_password)
            guest.role.shared_account = 1
            guest.role.guest = 1
            guest.save()
        return guest
