import subprocess
from django.templatetags.static import StaticNode as BaseStaticNode


class StaticNode(BaseStaticNode):
    def render(self, context):
        hash_val = subprocess.check_output(["git", "log", "-n", "1", "--pretty=%H"]).decode('utf-8').replace('\n', '')
        query_string = f'?hash={hash_val}'
        ret = super().render(context)
        if self.varname is None:
            return ret + query_string
        context[self.varname] = context[self.varname] + query_string
        return ""


def static(parser, token):
    return StaticNode.handle_token(parser, token)
