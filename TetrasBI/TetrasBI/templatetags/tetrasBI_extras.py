from django import template
from TetrasBI.models import Role
from django.utils.translation import gettext as _
from django.utils.text import format_lazy
from django.contrib.staticfiles import finders

from TetrasBI.models.upstream import UpstreamFactory

register = template.Library()


@register.filter('getClass')
def getClass(obj):
    return obj.__class__.__name__


@register.filter('roleString')
def roleString(user_roles):
    proxy_roles = [Role.ROLES.get(r, _('Unknown role')) for r in Role.ROLES if getattr(user_roles, r)]
    placeholders = ', '.join(['{}' for pr in proxy_roles])
    return format_lazy(placeholders, *proxy_roles) or _("Regular user")


@register.filter(name='loadingOrLogo')
def loadingOrLogo(loading_filepath):
    if finders.find(loading_filepath):
        return loading_filepath
    else:
        return loading_filepath.replace('loading', 'logo')


@register.filter(name='customOrDefault')
def customOrDefault(loading_filepath):
    if finders.find(loading_filepath):
        return loading_filepath
    else:
        return loading_filepath.replace('custom', 'default')


@register.filter(name='simpleConfiguration')
def simpleConfiguration(form):
    return [form_field for form_field in form if "advanced-configuration" not in form_field.field.widget.attrs.get('class', '')]


@register.filter(name='advancedConfiguration')
def advancedConfiguration(form):
    return [form_field for form_field in form if "advanced-configuration" in form_field.field.widget.attrs.get('class', '')]


@register.filter(name='getUpstreamUrlWithQueryString')
def getUpstreamUrlWithQueryString(upstream, user):
    return UpstreamFactory.getUpstream(upstream).getUpstreamUrlWithQueryString(user)
