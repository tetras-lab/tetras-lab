from django.db import migrations

from django.conf import settings

from TetrasBI.models.theme import Theme

from os import makedirs


def apply_migration(apps, schema_editor):
    makedirs(settings.THEME_ROOT, exist_ok=True)
    with open(settings.THEME_ROOT / 'theme.scss', 'w', encoding='UTF-8') as file:
        file.write("")
    Theme.update_assets()
    return True


class Migration(migrations.Migration):

    dependencies = [
        ('TetrasBI', '0016_auto_20221219_1056'),
    ]

    operations = [
        migrations.RunPython(apply_migration, migrations.RunPython.noop)
    ]
