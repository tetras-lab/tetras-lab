# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from urllib.parse import quote
from django.utils.translation import gettext as _
from os import environ
from django.conf import settings
from django.utils.translation import get_language


def handle(request):
    return TLContextProcessor().handle(request)


class TLContextProcessor:
    """ TLContextProcessor
    Any method added to this class will be used as a django context processor
    """
    def handle(self, request):
        attrs = [getattr(self, m) for m in dir(self) if m.startswith('process')]
        context = {}
        for m in attrs:
            if callable(m):
                context.update(m(request))
        return context

    def processSettings(self, request):
        return {
            "PROD": settings.PROD,
            "DEBUG": settings.DEBUG,
        }

    def processMailTo(self, request):
        email = environ.get('HELP_EMAIL', None)
        if email is not None:
            to = environ.get("HELP_EMAIL")
            subject = f'[ Tétras Lab ] {_("Support request")}'
            body = f'''{_("Hello")}

{_("I am encountering an issue with Tétras Lab")}

+ {_("I am trying to:")}

{_("Replace me with an explanation")}

+ {_("I see:")}

{_("Replace me with an explanation")}

+ {_("I Expected:")}

{_("Replace me with an explanation")}


## {_("Technical informations")}


+ {_("User:")} {request.user}
+ {_("URI:")} {request.build_absolute_uri()}
'''
            url = f'mailto:{to}?subject={quote(subject)}&body={quote(body)}'
        else:
            url = None
        return {
            'help_url': url
        }

    def processLocale(self, request):
        lang = get_language()
        return {
            'LOCALE': settings.LOCALES_CODE[lang],
            'LANG': lang
        }
