/*
 * Webpack d3components JavaScript file!
 *
 */

// load scss
import "./d3components.scss";

// custom js
import drawchart from '../js/chart_functions.js';

// // jquery
import $ from 'jquery';

// import d3 (after npm install)
// require('@node_modules/d3/build/d3.js');
// import * as d3 from "d3"; 

console.log('d3components.bundle loaded');
$(function(){
    $('.elocus-chart').each(function() {
		drawchart(this, {"anim":true});
	});
});