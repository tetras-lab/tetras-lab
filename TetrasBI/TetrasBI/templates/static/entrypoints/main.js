/*
 * Webpack main JavaScript file!
 *
 */

// load scss
import "./main.scss";

// any CSS you import will output into a single css file (?.css in this case)
// import './tetras-bi.css';

// jquery
import $ from 'jquery';

// custom js
global.tl = require('../js/tetras-bi.js');
import '../js/modals.js';
global.render = require('../js/render.js');
import '../js/cron_builder.js';

// bootstrap
import 'bootstrap';
import 'bootstrap4-toggle';
import 'bootstrap-toc/dist/bootstrap-toc';
import 'bootstrap-select/dist/js/bootstrap-select';

import 'select2/dist/js/select2.full.min.js';

console.log('main.bundle loaded');

$(function(){
});
