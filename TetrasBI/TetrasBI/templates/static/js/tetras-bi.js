// import $ from 'jquery';
// window.$ = $;
// global.$ = require("jquery");

import $, { removeData } from 'jquery';
import 'bootstrap-table/dist/bootstrap-table.js'

/**
 * Translates message
 */
export function _(message) {
    return gettext(message);
}

/**
 * Prints an alert message
 * level should be one of"info|success|warning|danger"
 */
export function alertGently(message, level="info") {
    var div = $('<div></div>');
    var html = '<button type="button" class="close" data-dismiss="alert" aria-label="close">×</button>';
    div.html(message+html);
    div.addClass('alert alert-'+level+' alert-dismissible show');
    div.attr('role', 'alert');
    $('#messages').append(div);
    setTimeout(
        function() {
            $(div).remove();
        },
        5000
    );
}

/**
 * Copy the value of input id to clipboard
 */
export function copyToClip(id) {
    navigator.clipboard.writeText($('#'+id).val()).then(function() {
        alertGently(_("Link copied to clipboard"));
    });
}

/**
 * Scrools to a selector
 */
export function scrollTo(selector) {
    var pos = $(selector).offset().top - 60
    $("body,html").animate(
      {
        scrollTop: pos
      },
      0 //speed
    );
}

/**
 * Toggles the overflow from visible to hidden
 */
export function toggleOverflow(dom) {
    var mode = $(dom).css('overflow-y') == "hidden" ? "visible" : "hidden";
    $(dom).css('overflow-y', mode);
}

export function scrollToTop() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

/**
 * Put a listener on a jquery event
 */
export function jQueryEvent(event, selector, handler) {
    $(selector).on(event, handler);
}

/**
 * Triggers a Jquery event on a dom
 */
export function triggerJqueryEvent(event, dom) {
    $(dom).trigger(event);
}

/**
 * Show the confirm modal
 * @param {string} question
 * @param {string} description (html allowed)
 * @param {string} postUrl
 * @param {object} dom : a dom to hide while the modal is shown
 */
export function confirmModal(question, description, postUrl, dom) {
    var modal = $('#confirmModal');
    modal.find('.tl-modal-title').text(question);
    modal.find('.tl-confirm-description').html(description);
    modal.find('form').attr('action', postUrl);
    if (typeof dom !== 'undefined') {
        $(dom).hide();
        modal.on('hidden.bs.modal', function() {
            $(dom).show();
        });
    }
    modal.modal('show');
}


$(function(){
    // From https://www.w3schools.com/bootstrap4/bootstrap_forms_custom.asp
    $(".tl-custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".tl-custom-file-label").addClass("selected").html(fileName);
    });
    // Toggle class is-pinned when context_nav is pinned see https://davidwalsh.name/detect-sticky
    const el = document.querySelector("#tl-sticky_container")
    const observer = new IntersectionObserver(
        ([e]) => e.target.classList.toggle("is-pinned", e.intersectionRatio < 1),
        { threshold: [1] }
    );
    observer.observe(el);
});


/**
 * Clear filtering field and display all filtered .tl-filterable DOM elements.
 * @param {inputSelector} DOM element to clear (input field).
 */
export function resetFilter(inputSelector) {
    let displaySelectors = ".tl-filterable";
    let hideSelector = ".tl-filterable-nothing";
    $(inputSelector).val("").trigger("change").focus();
    $(displaySelectors).show();
    $(hideSelector).hide();
    $('.bootstrap-table table.table').bootstrapTable('resetSearch');
}

/**
 * Filter .tl-filterable DOM elements depending on one or more words wrapped in .tl-filterable-content DOM elements.
 *      .tl-filterable-content DOM elements must be a child of .tl-filterable DOM elements.
 *      .tl-filterable-nothing DOM elements will be displayed if the filter doesn't return anything.
 *
 * @param {wordsToFilter} string containing those words (blank space separator)
 */
export function filterDomElements(wordsToFilter) {
    let elementSelector = ".tl-filterable";
    let targetSelector = ".tl-filterable-content";
    let nothingDisplayedSelector = ".tl-filterable-nothing";

    let isSomethingDisplayed = false;
    $(elementSelector).each(function() {
        // corpus is the concatenated texts in which we check the given words
        let corpus = $(this).find(targetSelector).text().toLowerCase();
        // 1) for each word, check if it is mentioned by at least one doc of the corpus (OR statement)
        // 2) compute the conjunction of the previous booleans (AND statement)
        let isCardDisplayed = wordsToFilter.toLowerCase().split(" ")
            .map(word => corpus.indexOf(word) > -1)
            .reduce((cardBoolUntilNow, cardBool) => cardBoolUntilNow && cardBool, true)
        $(this).toggle(isCardDisplayed);

        isSomethingDisplayed |= isCardDisplayed;
    });
    if (typeof nothingDisplayedSelector != 'undefined') {
        !isSomethingDisplayed ? $(nothingDisplayedSelector).show() : $(nothingDisplayedSelector).hide();
    }
    $('.bootstrap-table table.table').bootstrapTable('resetSearch', wordsToFilter);
}

/**
 * Sort DOM elements depending on a given criterion and an order (ascending or descending).
 * The elements to sort are identified thanks to .tl-sortable class.
 * The sorting is done according to the content of .tl-sortable-criterion-<sortCriterionIdentifier> DOM element.
 * Note:
 *      .tl-sortable-criterion-<sortCriterionIdentifier> must be a child of .tl-sortable.
 *      .tl-sortable-persistent will be appended to the sorted list.
 *
 * @param {sortCriterionIdentifier} the identifier of the sorting criterion.
 * @param {order} ascending or descending.
 */
export function sortDomElements(sortCriterionIdentifier, order){
    let elementSelector = ".tl-sortable";
    let targetSelector = ".tl-sortable-criterion-" + sortCriterionIdentifier;
    let persistentSelector = ".tl-sortable-persistent";

    let ascending = order == "asc" ? true : false;
    var sorted = $(elementSelector).sort(function(a, b) {
        let compare = $(a).find(targetSelector).text().toLowerCase()
                       <= $(b).find(targetSelector).text().toLowerCase();
        return compare == ascending ? false : true;
    });
    if (typeof persistentSelector != 'undefined'){
        sorted.push($('body').find(persistentSelector)[0]);
    }
    $(elementSelector).parent().html(sorted);
}

/**
 * Toggle sort button value and class.
 * Call sort function after toggling.
 * @param {buttonSelector} DOM selector of the button.
 * @param {criterionSelector} DOM selector of the element containing the criterion value.
 */
export function toggleSort(buttonSelector, criterionSelector){
    $(buttonSelector).val($(buttonSelector).val() == 'asc' ? 'desc' : 'asc');
    $(buttonSelector).toggleClass("mdi-sort-alphabetical-ascending mdi-sort-alphabetical-descending");
    tl.sortDomElements($(criterionSelector).val(), $(buttonSelector).val())
}

/**
 * Append options to the select element of the sorting form.
 * Walk through the first sortable element to find sub-elements exposing a tl-sortable-criterion-<key> class.
 * Link previous key to data-sort-criterion-label value.
 */
$(function() {
    let keyRegExp = new RegExp(/.*tl-sortable-criterion-([^\s"']*)/);
    $(".tl-sortable")
        .first()
        .find("[class*='tl-sortable-criterion-']")
        .each(function(idx, element){
            $('#sort-criterion').append(
                $("<option/>", {
                    value: $(element).attr("class").match(keyRegExp)[1],
                    text: $(element).attr("data-sort-criterion-label")
                })
            );
        });
});
/**
 * Toggle view (dashboard grid <-> dashboard list).
 * Toggle the corresponding button value and class.
 * @param {selector} the DOM selector of the button.
 */
export function toggleView(selector){
    let viewMode =  $(selector).val() == "list" ? "grid" : "list";
    // Generate the list-view on the fly only if requested
    tl.displayListItems();
    if (viewMode == "list"){
        $("#tl-grid-view").addClass("tl-hidden");
        $("#tl-list-view").closest('.table-responsive').removeClass("tl-hidden");
    } else {
        $("#tl-grid-view").removeClass("tl-hidden");
        $("#tl-list-view").closest('.table-responsive').addClass("tl-hidden");
    }
    $(selector).val(viewMode);
    $(selector).toggleClass("mdi-view-grid mdi-format-list-bulleted-square");
}


/**
 * Copy DOM elements tagged with '.tl-in-list-view' class to a table.
 */
 export function displayListItems(){
    let table = $("#tl-list-view");
    if(table.bootstrapTable("getData").length ==0) {
        let data = [];
        let cols = [];
         $('#tl-list-view th').each(function(i, e) {
             cols.push($(e).data('field'));
         });
        $("#tl-grid-view").find(".tl-card").each(function(idx, card){
            let entry = {};
            $(card).find(".tl-in-list-view").each(function(idy, itemToDisplay) {
                let col_idx = idy;
                if ($(card).data('url')) {
                    col_idx += 1;
                    let button = $("<button/>", {
                        type: "button",
                        "class": "tl-btn tl-btn-primary ml-4",
                        onClick: "window.location='" + $(card).data("url") + "'"
                    }).append($("<i/>", {"class": "mdi mdi-monitor-dashboard mr-1"}));
                    entry[_('view')] = button[0].outerHTML;
                }
                entry[cols[col_idx]] = $(itemToDisplay).html();
            });
            if (Object.keys(entry).length > 0){
                data.push(entry);
            }
        });
        table.bootstrapTable('prepend', data);
    }
    $('#filter').trigger('keyup');
 }


/**
 * Display an input file name to a file input field.
 * @param {selector} the DOM selector of the button.
 */
export function fileInputPlaceholder(selector){
    $(selector).parent().find('.file-selected').html(
        $(selector).val().substring(
            $(selector).val().lastIndexOf('\\') + 1
        )
    );
}


/**
 * Show a confirm modal when reseting the theme from admin page.
 */
export function confirmThemeReset(){
    tl.confirmModal(
        tl._("Do you want to reset the theme ?"),
        tl._("The images and colors will be overwritten by those of the default theme.")
        + "<br />" + "<strong>" + tl._("This change is irrecoverable") + "</strong>",
        "/admin/theme/reset"
    );
}


 /**
  * Initialize bootstrap tooltips
  */
 $(function() {
     $("body").tooltip({ selector: '[data-toggle=tooltip]' });
 });

  /**
  * Alert messages fading (for Django messages purpose)
  */
   $(function() {
    setTimeout(x => $("#messages .alert").hide(), 5000);
 });