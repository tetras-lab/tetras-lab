export function toggleCodeCells(withAlert=true) {
    if (renderer == 'nbviewer') 
        var cells=$('.code_cell .input')
    else
        var cells=$('.jp-InputArea').add('.jp-mod-noOutputs')
    
    if (sourcesVisible)
        cells.toggle()
    else
        cells.remove()

    if (withAlert) {
        if (cells.is(":hidden")) {
            tl.alertGently(tl._("Source code hidden"));
        } else {
            tl.alertGently(tl._("Source code shown"));
        }
    }
};    

/*
 * Respawns a callback function every seconds for 30 seconds while the function returns false
 */
 function retry(callback, numtries) {
    var max = 30;
    var waitms = 1000;
    if(!callback() && numtries < max) {
        setTimeout(function(){retry(callback, numtries+1)}, waitms);
    }
}

/*
 * Clear all direct position recursively
 */
 function clearPosition(doms) {
    doms.each(function(i, e) {
        e.style['position'] = ''
        e.style['height'] = ''
        clearPosition($(e).children());
    });
}

/*
 * Move all selectors with 'tetras-lab-quick-nav' class to the sticky bar
 */
 function initStickyBar() {
    var elems = $('.tetras-lab-quick-nav');
    if(elems.length == 0) {
        return false;
    }
    elems.appendTo($('#context_nav'));
    clearPosition(elems);
    return true;
}

/*
 * Tries to init the toc every seconds for maximum 30 seconds after loading contents
*/
function initToc() {
    var navSelector = "#tl-toc";
    Toc.init($(navSelector));
    if ($('#tl-toc .nav-link').length ==0) {
        return false;
    }
    $("body").scrollspy({
        target: navSelector
    });
    return true;
}


export default function render(url) {
    console.log("Rendering " + url);
    $('#spinner').show();
    $('#render').html("");
    $('#render').hide();
    $('.tetras-lab-quick-nav').remove();
    $('#tl-toc').html("");
    // Retrieve rendered notebook
    $.ajax({
        method: "GET",
            url: url,
    }).done(function( html ) {
        $('#render').html(html);

        if (renderer == 'nbviewer') {
            // Remove main menu, navigation and footer
            $('#menubar').remove();
            //$('.breadcrumb').remove();
            $('footer.hidden-print').remove();
            // Force our favicon instead of nbviewer one
            $('link[rel="shortcut icon"]').attr('href', $('link[rel="icon"]').attr('href'));
        }
        toggleCodeCells(false);
        $('a[href^="/' + renderer + '"]').each(function(e,v){
            var dom = $(v);
            var url = dom.attr("href");
            dom.attr("href", "#");
            dom.attr("url", url);
            dom.click(function(e){
                e.preventDefault();
                render($(e.target).attr("url"));
            });
        });
        $('#spinner').hide();
        $('#render').show();

        // Add bootstrap TOC, move actions to sticky bar and change hyperlinks
        retry(initToc, 0)
        retry(initStickyBar, 0)
    }).fail(function() {
        console.log("Failed");
        render(pathSafe)
    });
}
