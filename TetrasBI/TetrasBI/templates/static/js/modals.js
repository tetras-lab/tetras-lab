import $ from "jquery";

$().ready(function (){

    class AbstractModalForm {
        constructor(modal) {
            this.modal = modal;
            this.form = null;
            this.redirectUrl = "";
        }
        getRoute(){
            throw new Error('This function must be implemented');
        }
        getContentByAjax(){
            return $.get({url: this.getRoute()});
        }
        postModalForm(){
            this.modal.find('.tl-modal').modal('toggle');
            var self = this;
            return $.post({
                url: self.getRoute(),
                processData: false,
                contentType: false,
                data: new FormData(self.form[0])
            });
        }
        initializeModalContent(data){
            this.modal.html(data);
            this.form = this.modal.find('form');
            this.modal.find("[data-toggle='toggle']").bootstrapToggle();
            this.preProcessing();
            this.registerEvents();
            this.registerSubmitEvent();
            this.postProcessing();
            this.modal.find('.tl-modal').modal('toggle');
        }
        preProcessing(){
            // do nothing
        }
        selectPathsPreProcessing(target){
            // Bootstrap modal issue : https://stackoverflow.com/questions/18487056/select2-doesnt-work-when-embedded-in-a-bootstrap-modal/19574076#19574076
            $.fn.modal.Constructor.prototype._enforceFocus = function() {};
            $(this.modal.find(target)).select2({
                placeholder: tl._("Select a file or a folder..."),
                tags: true,
                data: $(this.modal.find(target)).data('options'),
                dropdownAutoWidth: true,
                selectionCssClass: ':all:',
                width: '1%'
            });
        }
        registerEvents(){
            // do nothing
        }
        registerSubmitEvent(){
            var self = this;
            $(this.form).submit(function(event){
                event.preventDefault();
                self.postModalForm()
                    .done(function(data, textStatus, jqXHR){
                        let status = $(data).filter('#status')[0].getAttribute('data-status');
                        status === 'error' ? self.initializeModalContent(data) : window.location.replace(self.redirectUrl);
                    })
                    .fail(function(jqXHR, textStatus, errorThrown){
                        tl.alertGently(errorThrown, "danger");
                    });
            });
        }
        postProcessing(){
            // do nothing;
        }

        escapeHtml(unsafe) {
            return $('<div>').text(unsafe).html();
        }
    }

    class DashboardCreateModalForm extends AbstractModalForm {
        constructor(modal) {
            super(modal);
            this.redirectUrl = "/dashboard/";
        }
        getRoute(){
            return '/dashboard/add';
        }
        preProcessing(){
            super.selectPathsPreProcessing('#id_path');
        }
        registerEvents(){
            this.registerImageSelectEvent();
        }
        registerImageSelectEvent() {
            let imgSrcPlaceholder = this.modal.find('#_img');
            var self = this;
            imgSrcPlaceholder.change(function () {
                self.modal.find('.file-selected').html(
                    imgSrcPlaceholder.val().substring(
                        imgSrcPlaceholder.val().lastIndexOf('\\') + 1
                    )
                );
            });
        }
        postProcessing(){
            this.modal.find('button[name="delete"]').hide();
        }
    }

    class DashboardUpdateModalForm extends DashboardCreateModalForm {
        constructor(modal, dashId) {
            super(modal);
            this.dashId = dashId;
        }
        getRoute(){
            return `/dashboard/${this.dashId}/update`;
        }
        registerEvents(){
            super.registerEvents();
            this.registerDeleteEvent();
        }
        registerDeleteEvent(){
            var self = this;
            this.modal.find('button[name="delete"]').click(function(event){
                let url = `/dashboard/${self.dashId}/delete`;
                let name = self.escapeHtml(self.modal.find('input[name="title"]').val());
                tl.confirmModal(
                    tl._("Do you want to delete this dashboard ?"),
                    tl._("The following dashboard will be removed: ") + "&nbsp;<code>" + name + "</code>"
                    + "<br />" + tl._("Share links created by any user will be lost, the file(s) will be kept.")
                    + "<br />" + "<strong>" + tl._("This change is irrecoverable") + "</strong>",
                    url,
                    self.modal,
                );
            });
        }
        postProcessing(){
            // do nothing
        }
    }

    class DashboardShareModalForm extends AbstractModalForm {
        constructor(modal, dashId) {
            super(modal);
            this.dashId = dashId;
            this.redirectUrl = "/dashboard/";
        }
        getRoute() {
            return `/dashboard/${this.dashId}/share`;
        }
        preProcessing(){
            this.modal.find('select[name="explicitViewers"]').selectpicker('refresh');
        }
        registerEvents() {
            this.registerPublicLinkUpdateEvent();
            this.registerSharedAllUpdateEvent();
        }
        registerPublicLinkUpdateEvent(){
            var self = this;
            this.modal.find('input[name="public_link"]').change(function(event) {
                let checkboxToken = self.modal.find('input#public_link_checkbox');
                let linkToken = self.modal.find('input#token').parent().find('a');
                checkboxToken.prop('checked') == true ? linkToken.show() : linkToken.hide();
            });
        }
        registerSharedAllUpdateEvent(){
            var self = this;
            this.modal.find('input[name="sharedAll"]').change(function(event) {
                let allUsers = self.modal.find('input#sharedAll');
                let someUsers = self.modal.find('select#id_explicitViewers');
                allUsers.prop('checked') == true ? someUsers.prop('disabled', true) : someUsers.prop('disabled', false);
                someUsers.selectpicker('refresh');
            });
        }
    }

    class UserCreateModalForm extends AbstractModalForm {
        constructor(modal) {
            super(modal);
            this.redirectUrl = "/admin/users";
        }
        getRoute(){
            return '/admin/user/add';
        }
        preProcessing(){
            this.modal.find('select[name="role"]').selectpicker('refresh');
        }
        postProcessing(){
            this.modal.find('button[name="delete"]').hide();
        }
    }

    class UserUpdateModalForm extends UserCreateModalForm {
        constructor(modal, userId) {
            super(modal);
            this.userId = userId;
        }
        getRoute(){
            return `/admin/user/${this.userId}`;
        }
        registerEvents(){
            super.registerEvents();
            this.registerDeleteEvent();
        }
        registerDeleteEvent(){
            var self = this;
            this.modal.find('button[name="delete"]').click(function(event){
                let url = `/admin/user/${self.userId}/delete`;
                let name = self.escapeHtml(self.modal.find('input[name="username"]').val());
                tl.confirmModal(
                    tl._("Do you want to delete this user ?"),
                    tl._("The following user will be removed: ") + "&nbsp;<code>" + name + "</code>"
                    + "<br />" + tl._("All user related data will be removed.")
                    + "<br />" + "<strong>" + tl._("This change is irrecoverable") + "</strong>",
                    url,
                    self.modal,
                );
            });
        }
        postProcessing(){
            // do nothing
        }
    }

    class JobDefinitionCreateModalForm extends AbstractModalForm {
        constructor(modal) {
            super(modal);
            this.redirectUrl = "/scheduling/";
        }
        getRoute(){
            return '/scheduling/job_definition/add';
        }
        preProcessing(){
            this.cronBuilderPreProcessing();
            super.selectPathsPreProcessing('#id_input_uri');
        }
        cronBuilderPreProcessing(){
            $(this.modal).find('#collapse-advanced-configuration').before(
                `<div class="tl-input-group  mb-3 mt-3">
                    <div class="input-group-prepend mr-1">
                        <label for="cron_builder" class="tl-input-group-text">
                            <i class="mdi mdi-clock-outline mr-1"></i>`+tl._("Scheduling")+`
                        </label>
                    </div>
                    <div id="cron_builder" name="cron_builder" class="form-control"></div>
                  </div>`
            );
            $(this.modal).find('div[name="cron_builder"]').height("auto").cronBuilder();
        }
        registerEvents(){
            super.registerEvents();
            this.registerCronBuilderEvent();
            this.registerCollapseEvent();
        }
        registerCronBuilderEvent(){
            var self = this;
            $(this.modal).find(".cron-input select, .cron-input input, .cron-select-period select").change(function(event){
                var expression = $(self.modal.find('div[name="cron_builder"]')).data('cronBuilder').getExpression();
                $(self.modal).find('input[name="schedule"]').val(expression);
            });
        }
        registerCollapseEvent(){
            var self = this;
            $(this.modal).find("#collapse-advanced-configuration").click(function(event){
                $(this).find("i.mdi").toggleClass("mdi-chevron-right mdi-chevron-down");
            });
        }
        postProcessing(){
            this.modal.find('button[name="delete"]').hide();
            $(this.modal).find('.cron-input select, .cron-input input, .cron-select-period select').addClass('tl-btn tl-btn-outline-primary');
            $(this.modal).find('div[name="cron_builder"] select').attr('style', 'min-width: 0px !important');
            $(this.modal).find('div[name="cron_builder"] input').attr('style', 'min-width: 0px !important');
            $(this.modal).find('input[name="schedule"]').val("* * * * *");
        }
    }

    class JobDefinitionUpdateModalForm extends JobDefinitionCreateModalForm {
        constructor(modal, jobDefId) {
            super(modal);
            this.jobDefId = jobDefId;
        }
        getRoute(){
            return `/scheduling/job_definition/${this.jobDefId}/update`;
        }
        preProcessing(){
            super.preProcessing();
            var cronString = $(this.modal).find('input[name="schedule"]').val().trim();
            $(this.modal.find('div[name="cron_builder"]')).data('cronBuilder').decodeExpression(cronString);
        }
        registerEvents(){
            super.registerEvents();
            this.registerDeleteEvent();
        }
        registerDeleteEvent(){
            var self = this;
            this.modal.find('button[name="delete"]').click(function(event){
                let url = `/scheduling/job_definition/${self.jobDefId}/delete`;
                let name = self.escapeHtml(self.modal.find('input[name="name"]').val());
                tl.confirmModal(
                    tl._("Do you want to delete this job definition ?"),
                    tl._("The following job definition will be removed: ") + "&nbsp;<code>" + name + "</code>"
                    + "<br />" + tl._("Jobs associated with this job definition will be lost.")
                    + "<br />" + "<strong>" + tl._("This change is irrecoverable") + "</strong>",
                    url,
                    self.modal,
                );
            });
        }
        postProcessing(){
            $(this.modal).find('.cron-input select, .cron-input input, .cron-select-period select').addClass('tl-btn tl-btn-outline-primary');
            $(this.modal).find('div[name="cron_builder"] select').attr('style', 'min-width: 0px !important');
            $(this.modal).find('div[name="cron_builder"] input').attr('style', 'min-width: 0px !important');
        }
    }

    class JobOutputModalForm extends AbstractModalForm {
        constructor(modal, jobId) {
            super(modal);
            this.jobId = jobId;
            this.redirectUrl = "/scheduling/";
        }
        getRoute(){
            return `/scheduling/job/${this.jobId}`;
        }
        preProcessing(){
            $(this.modal).find('.modal-dialog').attr('style', 'max-width: 90%; max-height: 90%;');
        }
        registerEvents(){
            super.registerEvents();
            this.registerDeleteEvent();
        }
        registerDeleteEvent(){
            var self = this;
            this.modal.find('button[name="delete"]').click(function(event){
                let url = `/scheduling/job/${self.jobId}/delete`;
                let name = self.escapeHtml(self.modal.find('h5.tl-modal-title').val());
                tl.confirmModal(
                    tl._("Do you want to delete this job ?"),
                    tl._("The following job will be removed: ") + "&nbsp;<code>" + name + "</code>"
                    + "<br />" + "<strong>" + tl._("This change is irrecoverable") + "</strong>",
                    url,
                    self.modal,
                );
            });
        }
        postProcessing() {
            this.modal.find('button[type="submit"]').hide();
        }
    }

    class JobDefinitionRunNowModalForm extends AbstractModalForm {
        constructor(modal, jobDefId) {
            super(modal);
            this.jobDefId = jobDefId;
            this.redirectUrl = "/scheduling/";
        }
        getRoute() {
            return `/scheduling/job_definition/${this.jobDefId}/run`;
        }
        postProcessing() {
            this.modal.find('button[name="delete"]').hide();
            this.modal.find('button[type="submit"]').html("<i class='mdi mdi-cog mr-1'></i>" + tl._("Run now"));
        }
    }

    // Load and display forms in a modal
    let modal = $('#modal-container');
    // Modal Factory
    function modalFactory(action, event) {
        let objectId = null;
        if (action === 'select_job') {
            objectId = $(event.target.selectedOptions).attr('id').split('_').pop();
        } else {
            objectId = event.target.id.split('_').pop();
        }
        if (!!(objectId)) {
            if (action === 'update_dashboard' && objectId == 0) {
                return new DashboardCreateModalForm(modal);
            } else if (action === 'update_dashboard') {
                return new DashboardUpdateModalForm(modal, objectId);
            } else if (action === 'share_dashboard') {
                return new DashboardShareModalForm(modal, objectId);
            } else if (action === 'update_user' && objectId == 0) {
                return new UserCreateModalForm(modal);
            } else if (action === 'update_user') {
                return new UserUpdateModalForm(modal, objectId);
            } else if (action === 'update_job_definition' && objectId == 0) {
                return new JobDefinitionCreateModalForm(modal);
            } else if (action === 'update_job_definition') {
                return new JobDefinitionUpdateModalForm(modal, objectId);
            } else if (action === 'run_job_now') {
                return new JobDefinitionRunNowModalForm(modal, objectId);
            } else if (action === 'view_job' || action === 'select_job') {
                return new JobOutputModalForm(modal, objectId);
            }
        }
    }
    function loadModal(actionModal){
        actionModal.getContentByAjax()
            .done(function (modalContent) {
                actionModal.initializeModalContent(modalContent);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                tl.alertGently(errorThrown, "danger");
            });
    }
    // List of actions that trigger modal loading
    ['update_dashboard', 'share_dashboard', 'update_user', 'update_job_definition', 'run_job_now', 'view_job'].forEach(function(action){
        $('html').on('click', '[id^='+action+'_]', function(event) {
            let actionModal = modalFactory(action, event);
            loadModal(actionModal);
        });
    });
    // Special case of job selection
    $('select[name="select_job"]').on('change', function(event){
        let action = 'select_job';
        let actionModal = modalFactory(action, event);
        loadModal(actionModal);
    });
});
