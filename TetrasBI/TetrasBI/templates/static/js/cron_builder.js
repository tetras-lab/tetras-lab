/*
The MIT License (MIT)

Copyright (c) 2016 Julia Cai
https://sotux.github.io/jquery-cron-quartz/

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
;(function($) {
    "use strict";

    var cronInputs={
        period: '<div class="cron-select-period"><label></label><select class="cron-period-select"></select></div>',
        startTime: '<div class="cron-input cron-start-time">Start time <select class="cron-clock-hour"></select>:<select class="cron-clock-minute"></select></div>',
        container: '<div class="cron-input"></div>',
        minutes: {
            tag: 'cron-minutes',
            inputs: [ '<p>Every <select class="cron-minutes-select"></select> minutes(s)</p>' ]
        },
        hourly: {
            tag: 'cron-hourly',
            inputs: [ '<p><input type="radio" name="hourlyType" value="every"> Every <select class="cron-hourly-select"></select> hour(s)</p>',
                '<p><input type="radio" name="hourlyType" value="clock"> Every day at <select class="cron-hourly-hour"></select>:<select class="cron-hourly-minute"></select></p>']
        },
        daily: {
            tag: 'cron-daily',
            inputs: [ '<p><input type="radio" name="dailyType" value="every"> Every <select class="cron-daily-select"></select> day(s)</p>',
                '<p><input type="radio" name="dailyType" value="clock"> Every week day</p>']
        },
        weekly: {
            tag: 'cron-weekly',
            inputs: [ '<p><input type="checkbox" name="dayOfWeekMon"> Monday<br /><input type="checkbox" name="dayOfWeekTue"> Tuesday<br />'+
                '<input type="checkbox" name="dayOfWeekWed"> Wednesday<br /><input type="checkbox" name="dayOfWeekThu"> Thursday<br />' +
                '<input type="checkbox" name="dayOfWeekFri"> Friday<br /><input type="checkbox" name="dayOfWeekSat"> Saturday<br />'+
                '<input type="checkbox" name="dayOfWeekSun"> Sunday</p>' ]
        },
        monthly: {
            tag: 'cron-monthly',
            inputs: [ '<p><input type="radio" name="monthlyType" value="byDay"> Day <select class="cron-monthly-day"></select> of every <select class="cron-monthly-month"></select> month(s)</p>',
                '<p><input type="radio" name="monthlyType" value="byWeek"> The <select class="cron-monthly-nth-day"></select> ' +
                '<select class="cron-monthly-day-of-week"></select> of every <select class="cron-monthly-month-by-week"></select> month(s)</p>']
        }
    };

    var periodOpts=arrayToOptions(["Minutes", "Hourly", "Daily", "Weekly", "Monthly"]);
    var minuteOpts=rangeToOptions(1, 60);
    var hourOpts=rangeToOptions(1, 24);
    var dayOpts=rangeToOptions(1, 100);
    var minuteClockOpts=rangeToOptions(0, 59, true);
    var hourClockOpts=rangeToOptions(0, 23, true);
    var dayInMonthOpts=rangeToOptions(1, 31);
    var monthOpts=arrayToOptions(["January","February","March","April","May","June","July","August","September","October","November","December"],
                                    [1,2,3,4,5,6,7,8,9,10,11,12]);
    var monthNumOpts=rangeToOptions(1, 12);
    var nthWeekOpts=arrayToOptions(["First", "Second", "Third", "Forth"], [1,2,3,4]);
    var dayOfWeekOpts=arrayToOptions(["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday", "Sunday"], ["MON","TUE","WED","THU","FRI","SAT", "SUN"]);


    // Convert an array of values to options to append to select input
    function arrayToOptions(opts, values) {
        var inputOpts='';
        for (var i = 0; i < opts.length; i++) {
            var value=opts[i];
            if(values!=null) value=values[i];
            inputOpts += "<option value='"+value+"'>" + opts[i] + "</option>\n";

        }
        return inputOpts;
    }

    // Convert an integer range to options to append to select input
    function rangeToOptions(start, end, isClock) {
        var inputOpts='', label;
        for (var i = start; i <= end; i++) {
            if(isClock && i < 10) label = "0" + i;
            else label = i;
            inputOpts += "<option value='"+i+"'>" + label + "</option>\n";
        }
        return inputOpts;
    }

    // Add input elements to UI as defined in cronInputs
    function addInputElements($baseEl, inputObj, onFinish) {
        $(cronInputs.container).addClass(inputObj.tag).appendTo($baseEl);
        $baseEl.children("."+inputObj.tag).append(inputObj.inputs);
        if(typeof onFinish === "function") onFinish();
    }

    var eventHandlers={
        periodSelect: function() {
            var period=($(this).val());
            var $selector=$(this).parent();
            $selector.siblings('div.cron-input').hide();
            $selector.siblings().find("select option").removeAttr("selected");
            $selector.siblings().find("select option:first").attr("selected", "selected");
            $selector.siblings('div.cron-start-time').show();
            $selector.siblings('div.cron-start-time').children("select.cron-clock-hour").val('12');
            switch(period) {
                case 'Minutes':
                    $selector.siblings('div.cron-minutes')
                        .show()
                        .find("select.cron-minutes-select").val('1');
                    $selector.siblings('div.cron-start-time').hide();
                    break;
                case 'Hourly':
                    var $hourlyEl=$selector.siblings('div.cron-hourly');
                    $hourlyEl.show()
                        .find("input[name=hourlyType][value=every]").prop('checked', true);
                    $hourlyEl.find("select.cron-hourly-hour").val('12');
                    $selector.siblings('div.cron-start-time').hide();
                    break;
                case 'Daily':
                    var $dailyEl=$selector.siblings('div.cron-daily');
                    $dailyEl.show()
                        .find("input[name=dailyType][value=every]").prop('checked', true);
                    break;
                case 'Weekly':
                    $selector.siblings('div.cron-weekly')
                        .show()
                        .find("input[type=checkbox]").prop('checked', false);
                    break;
                case 'Monthly':
                    var $monthlyEl=$selector.siblings('div.cron-monthly');
                    $monthlyEl.show()
                        .find("input[name=monthlyType][value=byDay]").prop('checked', true);
                    break;
            }
        }
    };

    // Public functions
    $.cronBuilder = function(el, options) {
        var base = this;

        // Access to jQuery and DOM versions of element
        base.$el=$(el);
        base.el=el;

        // Reverse reference to the DOM object
        base.$el.data('cronBuilder', base);

        // Initialization
        base.init = function() {
            base.options = $.extend({},$.cronBuilder.defaultOptions, options);

            base.$el.append(cronInputs.period);
            base.$el.find("div.cron-select-period label").text(base.options.selectorLabel);
            base.$el.find("select.cron-period-select")
                .append(periodOpts)
                .bind("change", eventHandlers.periodSelect);

            addInputElements(base.$el, cronInputs.minutes, function() {
                base.$el.find("select.cron-minutes-select").append(minuteOpts);
            });

            addInputElements(base.$el, cronInputs.hourly, function() {
                base.$el.find("select.cron-hourly-select").append(hourOpts);
                base.$el.find("select.cron-hourly-hour").append(hourClockOpts);
                base.$el.find("select.cron-hourly-minute").append(minuteClockOpts);
            });

            addInputElements(base.$el, cronInputs.daily, function() {
                base.$el.find("select.cron-daily-select").append(dayOpts);
            });

            addInputElements(base.$el, cronInputs.weekly);

            addInputElements(base.$el, cronInputs.monthly, function() {
                base.$el.find("select.cron-monthly-day").append(dayInMonthOpts);
                base.$el.find("select.cron-monthly-month").append(monthNumOpts);
                base.$el.find("select.cron-monthly-nth-day").append(nthWeekOpts);
                base.$el.find("select.cron-monthly-day-of-week").append(dayOfWeekOpts);
                base.$el.find("select.cron-monthly-month-by-week").append(monthNumOpts);
            });

            base.$el.append(cronInputs.startTime);
            base.$el.find("select.cron-clock-hour").append(hourClockOpts);
            base.$el.find("select.cron-clock-minute").append(minuteClockOpts);

            if(typeof base.options.onChange === "function") {
                base.$el.find("select, input").change(function() {
                    base.options.onChange(base.getExpression());
                });
            }

            base.$el.find("select.cron-period-select")
                .triggerHandler("change");

        }

        base.getExpression = function() {
            //var b = c.data("block");
            var sec = 0; // ignoring seconds by default
            var year = "*"; // every year by default
            var dow = "*"; // Modified, was "?"
            var month ="*", dom = "*";
            var min=base.$el.find("select.cron-clock-minute").val();
            var hour=base.$el.find("select.cron-clock-hour").val();
            var period = base.$el.find("select.cron-period-select").val();
            switch (period) {
                case 'Minutes':
                    var $selector=base.$el.find("div.cron-minutes");
                    var nmin=$selector.find("select.cron-minutes-select").val();
                	if(nmin > 1) min ="0/"+nmin;
                	else min="*";
                	hour="*";
                    break;

                case 'Hourly':
                    var $selector=base.$el.find("div.cron-hourly");
                    if($selector.find("input[name=hourlyType][value=every]").is(":checked")){
                        min=0;
                        hour="*";
                        var nhour=$selector.find("select.cron-hourly-select").val();
                    	if(nhour > 1) hour ="0/"+nhour;
                    } else {
                        min=$selector.find("select.cron-hourly-minute").val();
                        hour=$selector.find("select.cron-hourly-hour").val();
                    }
                    break;

                case 'Daily':
                    var $selector=base.$el.find("div.cron-daily");
                    if($selector.find("input[name=dailyType][value=every]").is(":checked")){
                        var ndom=$selector.find("select.cron-daily-select").val();
                        if(ndom > 1) dom ="1/"+ndom;
                    } else {
                        dom="*"; // Modified, was "?"
                        dow="MON-FRI";
                    }
                    break;

                case 'Weekly':
                    var $selector=base.$el.find("div.cron-weekly");
                    var ndow=[];
                    if($selector.find("input[name=dayOfWeekMon]").is(":checked"))
                        ndow.push("MON");
                    if($selector.find("input[name=dayOfWeekTue]").is(":checked"))
                        ndow.push("TUE");
                    if($selector.find("input[name=dayOfWeekWed]").is(":checked"))
                        ndow.push("WED");
                    if($selector.find("input[name=dayOfWeekThu]").is(":checked"))
                        ndow.push("THU");
                    if($selector.find("input[name=dayOfWeekFri]").is(":checked"))
                        ndow.push("FRI");
                    if($selector.find("input[name=dayOfWeekSat]").is(":checked"))
                        ndow.push("SAT");
                    if($selector.find("input[name=dayOfWeekSun]").is(":checked"))
                        ndow.push("SUN");
                    dow="*";
                    dom="*"; // Modified, was "?"
                    if(ndow.length < 7 && ndow.length > 0) dow=ndow.join(",");
                    break;

                case 'Monthly':
                    var $selector=base.$el.find("div.cron-monthly");
                    var nmonth;
                    if($selector.find("input[name=monthlyType][value=byDay]").is(":checked")){
                        month="*";
                        nmonth=$selector.find("select.cron-monthly-month").val();
                        dom=$selector.find("select.cron-monthly-day").val();
                        dow="*"; // Modified, was "?"
                    } else {
                        dow=$selector.find("select.cron-monthly-day-of-week").val()
                            +"#"+$selector.find("select.cron-monthly-nth-day").val();
                        nmonth=$selector.find("select.cron-monthly-month-by-week").val();
                        dom="*"; // Modified, was "?"
                    }
                    if(nmonth > 1) month ="1/"+nmonth;
                    break;

                default:
                    break;
            }
            return [min, hour, dom, month, dow].join(" ");
        };

        base.init();

        base.decodeExpression = function(cronString) {
            let expression_array = cronString.split(' ');
            let weekDay = expression_array.pop().trim();
            let month = expression_array.pop().trim();
            let day =  expression_array.pop().trim();
            let hour =  expression_array.pop().trim();
            let minute = expression_array.pop().trim();

            function capitalizeFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
            }

            function isInt(value) {
                return !isNaN(value) &&
                     parseInt(Number(value)) == value &&
                     !isNaN(parseInt(value, 10));
            }

            const comma_separate_regex = new RegExp('^(MON|TUE|WED|THU|FRI|SAT|SUN)(,(MON|TUE|WED|THU|FRI|SAT|SUN))*$');
            const hash_regex = new RegExp('^(MON|TUE|WED|THU|FRI|SAT|SUN)#\\d+$');

            if (minute == '*' && hour == '*' && day == '*' && month == '*' && weekDay == '*') {
                // * * * * * : every minute
                base.$el.find('.cron-period-select').val("Minutes").trigger("change");
                base.$el.find('.cron-minutes .cron-minutes-select').val("1");
            } else if (minute.startsWith('0/') && hour == '*' && day == '*' && month == '*' && weekDay == '*') {
                // 0/a * * * * : every a minutes
                base.$el.find('.cron-period-select').val("Minutes").trigger("change");
                base.$el.find('.cron-minutes .cron-minutes-select').val(minute.split("/").pop());
            } else if (minute == '0' && hour == '*' && day == '*' && month == '*' && weekDay == '*') {
                // 0 * * * * : every hour
                base.$el.find('.cron-period-select').val("Hourly").trigger("change");
                base.$el.find('.cron-hourly input[name="hourlyType"][value="every"]').prop("checked", true);
                base.$el.find('.cron-hourly .cron-hourly-select').val("1");
            } else if (minute == '0' && hour.startsWith('0/') && day == '*' && month == '*' && weekDay == '*') {
                // 0 0/a * * * : every a hour
                base.$el.find('.cron-period-select').val("Hourly").trigger("change");
                base.$el.find('.cron-hourly input[name="hourlyType"][value="every"]').prop("checked", true);
                base.$el.find('.cron-hourly .cron-hourly-select').val(hour.split("/").pop());
            } else if (isInt(minute) && isInt(hour) && day == '*' && month == '*' && weekDay == '*') {
                // a b * * * : every day at b hour and a minutes
                base.$el.find('.cron-period-select').val("Daily").trigger("change");
                base.$el.find('.cron-daily input[name="dailyType"][value="every"]').prop("checked", true);
                base.$el.find('.cron-daily .cron-daily-select').val("1");
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else if (isInt(minute) && isInt(hour) && day == '*' && month == '*' && weekDay == 'MON-FRI') {
                // a b * * MON-FRI : every week day at b hour and a minutes
                base.$el.find('.cron-period-select').val("Daily").trigger("change");
                base.$el.find('.cron-daily input[name="dailyType"][value="clock"]').prop("checked", true);
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else if (isInt(minute) && isInt(hour) && day == '*' && month == '*' && comma_separate_regex.test(weekDay)) {
                // a b * * ABC,DEF,... : every ABC, DEF day at b hour and a minutes
                base.$el.find('.cron-period-select').val("Weekly").trigger("change");
                weekDay.split(",").forEach(function (item){
                    base.$el.find('.cron-weekly input[name="dayOfWeek'+capitalizeFirstLetter(item)+'"]').prop("checked", true);
                });
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else if (isInt(minute) && isInt(hour) && day == '*' && month == '*' && hash_regex.test(weekDay)) {
                // a b * * ABC#c : the c-th ABC of every month at b hour and a minutes
                base.$el.find('.cron-period-select').val("Monthly").trigger("change");
                base.$el.find('.cron-monthly input[name="monthlyType"][value="byWeek"]').prop("checked", true);
                base.$el.find('.cron-monthly .cron-monthly-nth-day').val(weekDay.split("#").pop());
                base.$el.find('.cron-monthly .cron-monthly-day-of-week').val(weekDay.split("#").shift());
                base.$el.find('.cron-monthly .cron-monthly-month-by-week').val("1");
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else if (isInt(minute) && isInt(hour) && day == '*' && month.startsWith('1/') && hash_regex.test(weekDay)) {
                // a b * 1/c ABC#d : the d-th ABC of every c months at b hour and a minutes
                base.$el.find('.cron-period-select').val("Monthly").trigger("change");
                base.$el.find('.cron-monthly input[name="monthlyType"][value="byWeek"]').prop("checked", true);
                base.$el.find('.cron-monthly .cron-monthly-nth-day').val(weekDay.split("#").pop());
                base.$el.find('.cron-monthly .cron-monthly-day-of-week').val(weekDay.split("#").shift());
                base.$el.find('.cron-monthly .cron-monthly-month-by-week').val(month.split('/').pop());
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else if (isInt(minute) && isInt(hour) && day.startsWith('1/')  && month == '*' && weekDay == '*') {
                // a b 1/c * * : every c day at b hour and a minutes
                base.$el.find('.cron-period-select').val("Daily").trigger("change");
                base.$el.find('.cron-daily input[name="dailyType"][value="every"]').prop("checked", true);
                base.$el.find('.cron-daily .cron-daily-select').val(day.split("/").pop());
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else if (isInt(minute) && isInt(hour) && isInt(day)  && month == '*' && weekDay == '*') {
                // a b c * * : every day c at b hour and a minutes
                base.$el.find('.cron-period-select').val("Monthly").trigger("change");
                base.$el.find('.cron-monthly input[name="monthlyType"][value="byDay"]').prop("checked", true);
                base.$el.find('.cron-monthly .cron-monthly-day').val(day);
                base.$el.find('.cron-monthly .cron-monthly-month').val("1");
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else if (isInt(minute) && isInt(hour) && isInt(day)  && month.startsWith('1/') && weekDay == '*') {
                // a b c 1/d * : every c day of every d months at b hour and a minutes
                base.$el.find('.cron-period-select').val("Monthly").trigger("change");
                base.$el.find('.cron-monthly input[name="monthlyType"][value="byDay"]').prop("checked", true);
                base.$el.find('.cron-monthly .cron-monthly-day').val(day);
                base.$el.find('.cron-monthly .cron-monthly-month').val(month.split("/").pop());
                base.$el.find('.cron-start-time .cron-clock-hour').val(hour);
                base.$el.find('.cron-start-time .cron-clock-minute').val(minute);
            } else {
                console.error('Decoding failure of the CRON string: ' + cronString);
                base.$el.find('.cron-period-select').val("Minutes").trigger("change");
                base.$el.find('.cron-minutes .cron-minutes-select').val("1");
                base.$el.append("<b>"+tl._("Unable to decode CRON string. Please check its value in advanced settings.")+"</b>");
            }
        }
    };

    // Plugin default options
    $.cronBuilder.defaultOptions = {
        selectorLabel: "Period: "

    };

    // Plugin definition
    $.fn.cronBuilder = function(options) {
        return this.each(function(){
            (new $.cronBuilder(this, options));
        });
    };

}( jQuery ));