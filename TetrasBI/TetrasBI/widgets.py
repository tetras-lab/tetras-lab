from django.forms import widgets


class TetrasBIToggleSwitchWidget(widgets.CheckboxInput):
    template_name = "components/tetras_bi_toggle_switch_widget.html"

    def __init__(self, attrs=None, on_icon="", off_icon="", on_text="", off_text="", on_style="", off_style=""):
        self.on_icon = on_icon
        self.off_icon = off_icon
        self.on_text = on_text
        self.off_text = off_text
        self.on_style = on_style
        self.off_style = off_style
        super().__init__(attrs=attrs)

    def get_context(self, name, value, attrs=None):
        context = super().get_context(name, value, attrs)
        context.update({
            "on_icon": self.on_icon,
            "off_icon": self.off_icon,
            "on_text": self.on_text,
            "off_text": self.off_text,
            "on_style": self.on_style,
            "off_style": self.off_style,
        })
        return context


class TetrasBIFileInputWidget(widgets.ClearableFileInput):
    template_name = "components/tetras_bi_file_input_widget.html"

    def __init__(self, attrs=None, delete_label="", delete_icon="", delete_on_icon="", delete_off_icon="",
                 delete_on_text="", delete_off_text="", delete_on_style="", delete_off_style="", input_label="",
                 input_icon="", input_accept="", input_browse_text=""):
        self.delete_label = delete_label
        self.delete_icon = delete_icon
        self.delete_on_icon = delete_on_icon
        self.delete_off_icon = delete_off_icon
        self.delete_on_text = delete_on_text
        self.delete_off_text = delete_off_text
        self.delete_on_style = delete_on_style
        self.delete_off_style = delete_off_style
        self.input_label = input_label
        self.input_icon = input_icon
        self.input_accept = input_accept
        self.input_browse_text = input_browse_text
        super().__init__(attrs=attrs)

    def get_context(self, name, value, attrs=None):
        context = super().get_context(name, value, attrs)
        context.update({
            "delete_label": self.delete_label,
            "delete_icon": self.delete_icon,
            "delete_on_icon": self.delete_on_icon,
            "delete_off_icon": self.delete_off_icon,
            "delete_on_text": self.delete_on_text,
            "delete_off_text": self.delete_off_text,
            "delete_on_style": self.delete_on_style,
            "delete_off_style": self.delete_off_style,
            "input_label": self.input_label,
            "input_icon": self.input_icon,
            "input_accept": self.input_accept,
            "input_browse_text": self.input_browse_text
        })
        return context


class TetrasBIHiddenTextWithBooleanWidget(widgets.CheckboxInput):
    template_name = "components/tetras_bi_hidden_text_with_boolean_widget.html"

    def __init__(self, attrs=None, label="", icon="", base_url="", token="", copy_label="", copy_icon="", on_icon="", off_icon="",
                 on_text="", off_text="", del_text="", on_style="", off_style="", del_style=""):
        self.label = label
        self.icon = icon
        self.base_url = base_url
        self.token = token
        self.copy_label = copy_label
        self.copy_icon = copy_icon
        self.on_icon = on_icon
        self.off_icon = off_icon
        self.on_text = on_text
        self.off_text = off_text
        self.del_text = del_text
        self.on_style = on_style
        self.off_style = off_style
        self.del_style = del_style
        super().__init__(attrs=attrs)

    def get_context(self, name, value, attrs=None):
        context = super().get_context(name, value, attrs)
        context.update({
            "label": self.label,
            "icon": self.icon,
            "base_url": self.base_url,
            "token": self.token,
            "copy_label": self.copy_label,
            "copy_icon": self.copy_icon,
            "on_icon": self.on_icon,
            "off_icon": self.off_icon,
            "on_text": self.on_text,
            "off_text": self.off_text,
            "del_text": self.del_text,
            "on_style": self.on_style,
            "off_style": self.off_style,
            "del_style": self.del_style
        })
        return context


class TetrasBISelectMultipleWidget(widgets.SelectMultiple):
    template_name = "components/tetras_bi_select_multiple_widget.html"

    def __init__(self, attrs=None, select_all_text="", deselect_all_text="", title="", disabled="", options=""):
        self.select_all_text = select_all_text
        self.deselect_all_text = deselect_all_text
        self.title = title
        self.disabled = disabled
        self.options = options
        super().__init__(attrs=attrs)

    def get_context(self, name, value, attrs=None):
        context = super().get_context(name, value, attrs)
        context.update({
            "select_all_text": self.select_all_text,
            "deselect_all_text": self.deselect_all_text,
            "title": self.title,
            "disabled": self.disabled,
            "options": self.options,
        })
        return context


class TetrasBIImageInputWidget(widgets.ClearableFileInput):
    template_name = "components/tetras_bi_image_input_widget.html"

    def __init__(self, attrs=None, delete_label="", delete_icon="", delete_on_icon="", delete_off_icon="",
                 delete_on_text="", delete_off_text="", delete_on_style="", delete_off_style="", input_label="",
                 input_icon="", input_accept="", input_browse_text="", is_clearable=False, image_size=0, wrapper_classes=""):
        self.delete_label = delete_label
        self.delete_icon = delete_icon
        self.delete_on_icon = delete_on_icon
        self.delete_off_icon = delete_off_icon
        self.delete_on_text = delete_on_text
        self.delete_off_text = delete_off_text
        self.delete_on_style = delete_on_style
        self.delete_off_style = delete_off_style
        self.input_label = input_label
        self.input_icon = input_icon
        self.input_accept = input_accept
        self.input_browse_text = input_browse_text
        self.is_clearable = is_clearable
        self.image_size = image_size
        self.wrapper_classes = wrapper_classes
        super().__init__(attrs=attrs)

    def get_context(self, name, value, attrs=None):
        context = super().get_context(name, value, attrs)
        context.update({
            "delete_label": self.delete_label,
            "delete_icon": self.delete_icon,
            "delete_on_icon": self.delete_on_icon,
            "delete_off_icon": self.delete_off_icon,
            "delete_on_text": self.delete_on_text,
            "delete_off_text": self.delete_off_text,
            "delete_on_style": self.delete_on_style,
            "delete_off_style": self.delete_off_style,
            "input_label": self.input_label,
            "input_icon": self.input_icon,
            "input_accept": self.input_accept,
            "input_browse_text": self.input_browse_text,
            "is_clearable": self.is_clearable,
            "image_size": self.image_size,
            "wrapper_classes": self.wrapper_classes
        })
        return context