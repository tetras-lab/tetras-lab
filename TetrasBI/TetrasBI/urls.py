# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""TetrasBI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.static import static
from django.conf import settings
from django.views.i18n import JavaScriptCatalog
from django.views.generic.base import RedirectView
from .views import proxy, renderer, scheduling, users, registration
from .views.admin import AdminView, AdminThemeResetView
from .views.status import StatusView
import re

urlpatterns = [
    # Registration
    path('accounts/login/', registration.LoginView.as_view(), name="login"),
    path('accounts/logout/', registration.LogoutView.as_view(), name="logout"),
    path("accounts/password_reset/", registration.PasswordResetView.as_view(), name="password_reset"),
    path("accounts/password_reset/done/", registration.PasswordResetDoneView.as_view(), name="password_reset_done"),
    path("accounts/reset/<uidb64>/<token>/", registration.PasswordResetConfirmView.as_view(),
         name="password_reset_confirm"),
    path("accounts/reset/done/", registration.PasswordResetCompleteView.as_view(), name="password_reset_complete"),
    path('account/', users.EditAccountView.as_view(), name='account'),

    # Debug toolbar
    path('__debug__/', include(debug_toolbar.urls)),

    # Langs
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),


    # Scheduling
    path('scheduling/', scheduling.SchedulingListView.as_view(), name='scheduling'),
    path('scheduling/job_definition/add', scheduling.SchedulingCreateJobDefView.as_view(), name='scheduling-job-def-add'),
    path('scheduling/job_definition/<str:id>/update', scheduling.SchedulingUpdateJobDefView.as_view(), name='scheduling-job-def-update'),
    path('scheduling/job_definition/<str:id>/delete', scheduling.SchedulingDeleteJobDefView.as_view(), name='scheduling-job-def-delete'),
    path('scheduling/job_definition/<str:id>/run', scheduling.SchedulingRunNowJobDefView.as_view(), name='scheduling-job-def-run'),
    path('scheduling/job/<str:id>', scheduling.SchedulingReadJobView.as_view(), name='scheduling-job-read'),
    path('scheduling/job/<str:id>/delete', scheduling.SchedulingDeleteJobView.as_view(), name='scheduling-job-delete'),

    # Dashboards
    path('', RedirectView.as_view(url='/dashboard'), name='home'),
    path('dashboard/', include("dashboards.urls")),
    path('dashboard/public/<str:token>', renderer.publicDashboard, name='public_dashboard'),

    # Retro-compatibility
    path('public/<str:token>', RedirectView.as_view(url='/dashboard/public/%(token)s'), name='public-compatibility'),
    path('admin/jupyter', RedirectView.as_view(url='/jupyter/'), name='jupyter-compatibility'),

    # User management
    path('admin/users', users.UsersView.as_view(), name='users'),
    path('admin/user/<int:pk>', users.UserUpdateView.as_view(), name='user-update'),
    path('admin/user/<int:pk>/delete', users.UserDeleteView.as_view(), name='user-delete'),
    path('admin/user/<int:pk>/resetPW', users.UserResetPwView.as_view(), name='user-reset-pw'),
    path('admin/user/add', users.UserCreateView.as_view(), name='user-add'),
    path('admin/user/firstrun', users.UserCreateFirstView.as_view(), name='first-run'),
    # Impersonation
    path('impersonate/', include('impersonate.urls')),
    # Admin
    path('admin/', AdminView.as_view(), name='admin'),
    path('admin/theme/reset', AdminThemeResetView.as_view(), name='admin-theme-reset'),

    path('status/', StatusView.as_view(), name='status'),

]
# Static and Media files
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# The following path MUST always be the last one as it is a catch (almost) all proxy url
urlpatterns += [
    re_path(r"^(?P<proxy>[a-z]*)/(?P<url>.*)$", proxy.HttpProxy.as_view(), name='proxy'),
]

websocket_urlpatterns = [
    re_path(r"^(?P<proxy>[a-z]*)/(?P<url>.*)$", proxy.WebsocketProxy.as_asgi(), name='wsproxy'),
]
