# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.dispatch import receiver
from django.contrib.auth.models import User
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.db.models.signals import post_save
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
from django.core.mail import EmailMessage
from django.conf import settings
from django.template import loader
from django.urls import reverse
from impersonate.signals import session_begin, session_end

from .models.role import Role
from .models.upstream import UpstreamFactory

import json


def send_welcome_email(sender, instance, created, **kwargs):
    if created and instance.email:
        subject = _(f'Welcome {instance}')
        context = {
            'token': PasswordResetTokenGenerator().make_token(instance),
            'uid':  urlsafe_base64_encode(force_bytes(instance.pk)),
            'login': instance.username,
            'email': instance.email,
            'protocol': settings.PROTO,
            'domain': settings.EXTERNAL_HOST,
        }
        body = loader.render_to_string('registration/welcome_email.html', context)
        email_message = EmailMessage(subject, body, settings.DEFAULT_FROM_EMAIL, [instance.email])
        email_message.subtype = "html"
        email_message.send()


def create_user_role(sender, instance, created, **kwargs):
    if created:
        Role.objects.create(user=instance)


def build_workspace_payload(username):
    return json.loads('''{
        "data": {
            "layout-restorer:data": {
                "main": {"dock": {"type": "tab-area", "currentIndex": 0, "widgets": []}},
                "down": {"size": 0, "widgets": []},
                "left": {"collapsed": false, "visible": true, "current": "filebrowser",
                         "widgets": [
                                        "filebrowser",
                                        "running-sessions",
                                        "@jupyterlab/toc:plugin",
                                        "extensionmanager.main-view"
                                    ]
                        },
                "right": {"collapsed": true, "visible": true, "widgets": ["jp-property-inspector", "debugger-sidebar"]},
                "relativeSizes": [0.25, 0.75, 0],
                "top": {"simpleVisibility": true}
            },
            "file-browser-filebrowser:cwd": {"path": "dashboards/''' + username + '''"}
        },
        "metadata": {"id": "''' + username + '''"}
    }''')


def create_jupyterlab_workspace(instance):
    payload = build_workspace_payload(instance.username)
    jl = UpstreamFactory.getUpstream('jupyter')
    jl.apiCall("delete", "workspace", scheme='http', args={"username": instance.username})
    jl.apiCall("put", "workspace", payload=payload, scheme='http', args={"username": instance.username})


@receiver(post_save, sender=User)
def post_user_create(sender, instance, created, **kwargs):
    create_user_role(sender, instance, created, **kwargs)
    send_welcome_email(sender, instance, created, **kwargs)
    create_jupyterlab_workspace(instance)


@receiver(post_save, sender=User)
def save_user_role(sender, instance, **kwargs):
    instance.role.save()


@receiver(session_begin)
def impersonate_started(sender, impersonator, impersonating, request, **kwargs):
    messages.success(request, _('Impersonating {impersonating_username}')
                     .format(impersonating_username=impersonating.username))
    request.session['_impersonate_prev_path'] = reverse('users')


@receiver(session_end)
def impersonate_stopped(sender, impersonator, impersonating, request, **kwargs):
    messages.success(request, _('Impersonation finished, back on {impersonator_username}\'s session')
                     .format(impersonator_username=impersonator.username))
