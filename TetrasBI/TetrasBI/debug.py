# Copyright (C) 2021 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf import settings


def debug_is_safe(request=None):
    return settings.DEBUG and not settings.PROD

def toolbar_enabled(request=None):
    return debug_is_safe(request) and settings.DEBUG_TOOLBAR

def middleware_enabled():
    return False
    return settings.REMOTE_DEBUG['DEBUGGER'] == 'DBGP'

def brk(cond=True):
    if not debug_is_safe() or not cond:
        return
    if settings.REMOTE_DEBUG['DEBUGGER'] == 'DBGP':
        from dbgp.client import brk as brk_fct
    else:
        from pydevd_pycharm import settrace as brk_fct
    brk_fct(host=settings.REMOTE_DEBUG['HOST'], port=settings.REMOTE_DEBUG['PORT'])
