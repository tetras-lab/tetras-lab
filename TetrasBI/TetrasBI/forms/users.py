# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ChoiceField, MultipleChoiceField, SelectMultiple
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from .tlform import TlModelForm
from TetrasBI.models import Role


class UserInitialCreationForm(UserCreationForm, TlModelForm):

    class Meta(UserCreationForm.Meta):
        icons = {
            'username': 'account-outline',
            'email': 'email-outline',
            'password1': 'lock-outline',
            'password2': 'lock-check-outline',
        }


class UserEditSelfForm(UserChangeForm, TlModelForm):
    password = None
    lang = ChoiceField(
        choices=settings.LANGUAGES,
        label=_('Lang')
    )

    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']
        icons = {
            'username': 'account-outline',
            'email': 'email-outline',
            'first_name': 'account-details-outline',
            'last_name': 'account-details-outline',
            'lang': 'flag-outline',
        }


class UserEditForm(UserChangeForm, TlModelForm):
    password = None

    def __init__(self, *args, **kwargs):
        try:
            initial = kwargs.pop('initial_roles')
        except:
            initial = []
        super().__init__(*args, **kwargs)
        self.fields['role'] = MultipleChoiceField(
                choices=[(r, Role.ROLES.get(r)) for r in Role.ROLES if r != 'guest'],
                required=False,
                label='Roles',
                help_text=_(
                    'Data Scientist can create dashboards and use scientist tools.' +
                    '<br />Administrators can manage users.' +
                    '<br />Shared accounts are regular users who cannot update their account.'
                ),
                initial=initial,
                widget=SelectMultiple(attrs={
                    'class': 'selectpicker ml',
                    'data-width': '210',
                    'size': 'false',
                    'title': _('Select roles...'),
                    'aria-label': "multiple select"
                })
            )
        self.fields['role'].icon = 'shield-account-outline'
        self.order_fields(field_order=['username', 'first_name', 'last_name', 'email', 'role'])


    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']
        icons = {
            'username': 'account-outline',
            'email': 'email-outline',
            'first_name': 'account-details-outline',
            'last_name': 'account-details-outline',
        }
