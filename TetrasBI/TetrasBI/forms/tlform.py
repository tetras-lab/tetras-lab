# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import BaseForm


class TlModelForm(BaseForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        icons = getattr(self.Meta, 'icons', dict())
        placeholders = getattr(self.Meta, 'placeholders', dict())

        for field_name, field in self.fields.items():
            # Bootstrap classes
            field.widget.attrs['class'] = field.widget.attrs.get('class', '') + ' form-control'
            # Icons (mdi)
            if field_name in icons:
                field.icon = icons[field_name]
            # Placeholders
            if field_name in placeholders:
                field.widget.attrs['placeholder'] = placeholders[field_name]

