# Copyright (C) 2020 Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth import forms as base
from .tlform import TlModelForm


class LoginForm(base.AuthenticationForm, TlModelForm):
    class Meta():
        fields = ['username', 'password']
        icons = {
            'username': 'account-outline',
            'password': 'lock-outline',
        }


class PasswordResetForm(base.PasswordResetForm, TlModelForm):
    class Meta():
        icons = {
            'email': 'email-outline',
        }


class SetPasswordForm(base.SetPasswordForm, TlModelForm):
    class Meta():
        icons = {
            'new_password1': 'lock-outline',
            'new_password2': 'lock-check-outline',
        }


class PasswordChangeForm(base.PasswordChangeForm, TlModelForm):
    class Meta():
        icons = {
            'old_password': 'shield-lock-outline',
            'new_password1': 'lock-outline',
            'new_password2': 'lock-check-outline',
        }
