from django import forms

from django.utils.translation import gettext_lazy as _

from dashboards.models.dashboard import Dashboard

from TetrasBI.widgets import TetrasBIToggleSwitchWidget

from pathlib import Path


class SchedulingEditJobDefinition(forms.Form):
    form_fields = ['name', 'old_input_uri', 'input_uri', 'active', 'output_formats', 'parameters', 'tags', 'schedule',
                   'runtime_environment_name', 'output_filename_template', 'timezone']

    def __init__(self, *args, **kwargs):
        self.create_or_update = kwargs.pop('create_or_update', 'create')
        super().__init__(*args, **kwargs)

        # name field is not updatable
        if self.create_or_update == "create":
            self.fields["name"] = forms.CharField(
                label=_("Job definition name"),
                required=True,
                widget=forms.TextInput(attrs={"class": "form-control"}),
            )
            self.fields["name"].icon = "format-title"
        else:
            self.fields["name"] = forms.CharField(widget=forms.HiddenInput())

        # if updating, input_uri is not required but the old input is shown
        choices = [('', '--')] + [
            (v, v) for (v, v) in enumerate(Dashboard.objects.getPotentialDashboards(
                allow_existing=True, exclude=['tl_hidden.ipynb', 'tl_jobs_hidden.ipynb'])
            ) if v.endswith("ipynb")]
        if self.create_or_update == "update":
            self.fields["old_input_uri"] = forms.ChoiceField(
                label=_("Current file"),
                required=False,
                choices=[(Path(c[0]).name, Path(c[1]).name) for c in choices],
                widget=forms.Select(attrs={"class": "form-control"})
            )
            self.fields["old_input_uri"].disabled = True
            self.fields["old_input_uri"].icon = "file-outline"
        self.fields["input_uri"] = forms.ChoiceField(
            label=_("Input file"),
            required=True,
            choices=choices,
            widget=forms.Select(attrs={"class": "form-control"})
        )
        self.fields["input_uri"].icon = "file-search-outline"
        if self.create_or_update == "update":
            self.fields["input_uri"].required = False
            self.fields["input_uri"].label = _("Update input file")

        # active field is only updatable
        if self.create_or_update == "update":
            self.fields["active"] = forms.BooleanField(
                label=_("Activate"),
                required=False,
                widget=TetrasBIToggleSwitchWidget(
                    on_icon="<i class='mdi mdi-play mr-1'></i>",
                    off_icon="<i class='mdi mdi-pause mr-1'></i>",
                    on_text=_("Run"),
                    off_text=_("Pause"),
                    on_style="success",
                    off_style="secondary",
                )
            )
            self.fields["active"].icon = "play-pause"

        # schedule field: CRON string
        self.fields["schedule"] = forms.CharField(
            label=_("Cron string"),
            required=True,
            widget=forms.TextInput(attrs={"class": "form-control advanced-configuration"})
        )
        self.fields["schedule"].icon = "clock-outline"

        # parameters field is not updatable
        if self.create_or_update == "create":
            self.fields["parameters"] = forms.CharField(
                label=_("Parameters"),
                required=False,
                widget=forms.Textarea(attrs={
                    "class": "form-control advanced-configuration",
                    "rows": 3,
                    "placeholder": '#Examples (one per row):\ncolor = "blue"\nmax_iterations = 500'
                })
            )
            self.fields["parameters"].icon = "cogs"
        else:
            self.fields["parameters"] = forms.CharField(widget=forms.HiddenInput(), required=False)

        # tags field is not updatable
        if self.create_or_update == "create":
            self.fields["tags"] = forms.CharField(
                label=_("Tags"),
                required=False,
                widget=forms.Textarea(attrs={
                    "class": "form-control advanced-configuration",
                    "rows": 1,
                    "placeholder": 'firstTag, secondTag'
                })
            )
            self.fields["tags"].icon = "tag-multiple"
        else:
            self.fields["tags"] = forms.CharField(widget=forms.HiddenInput(), required=False)

        self.fields["output_formats"] = forms.CharField(widget=forms.HiddenInput())
        self.fields["runtime_environment_name"] = forms.CharField(widget=forms.HiddenInput())
        self.fields["output_filename_template"] = forms.CharField(widget=forms.HiddenInput())
        self.fields["timezone"] = forms.CharField(widget=forms.HiddenInput())


class SchedulingEditJob(forms.Form):

    def __init__(self, *args, **kwargs):
        self.html_output = kwargs.pop('html_output')
        super().__init__(*args, **kwargs)


class SchedulingCreateJob(forms.Form):
    form_fields = ['job_definition_id', 'parameters', 'name']

    def __init__(self, *args, **kwargs):
        self.create_or_update = kwargs.pop('create_or_update', 'create')
        super().__init__(*args, **kwargs)
        self.fields["parameters"] = forms.CharField(
            label=_("Parameters"),
            required=False,
            widget=forms.Textarea(attrs={
                "class": "form-control",
                "rows": 3,
                "placeholder": '#Examples (one per row):\ncolor = "blue"\nmax_iterations = 500'
            })
        )
        self.fields["parameters"].icon = "cogs"
        self.fields["job_definition_id"] = forms.CharField(widget=forms.HiddenInput())
        self.fields["name"] = forms.CharField(widget=forms.HiddenInput())
