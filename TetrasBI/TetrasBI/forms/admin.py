from django import forms

from django.core.files import File
from django.core.validators import FileExtensionValidator

from django.utils.translation import gettext_lazy as _

from colorfield.widgets import ColorWidget

from TetrasBI.fields import SVGAndImageFormField
from TetrasBI.widgets import TetrasBIImageInputWidget


class ThemeEditForm(forms.Form):
    color_fields = ['primaryBg', 'secondaryBg', 'dangerBg', 'fg', 'greyed', 'chart0', 'chart1', 'chart2', 'chart3']
    image_fields = ['favicon', 'logo', 'loading', 'undraw_dashboard']
    labels = {
        'primaryBg': _('Background primary'), 'secondaryBg': _('Background secondary'),
        'dangerBg': _('Background danger'),
        'fg': _('Foreground'), 'greyed': _('Grey'), 'chart0': _('Curve'), 'chart1': _('Bar chart #1'),
        'chart2': _('Bar chart #2'),
        'chart3': _('Bar chart #3'), 'favicon': _('Favicon (ico)'), 'logo': _('Logo (png)'),
        'loading': _('Loading logo (png)'),
        'undraw_dashboard': _('Default dashboard (svg)')
    }
    images_parameters = {
        'favicon': {'size': 64, 'type': 'image/x-icon', 'extension': 'ico'},
        'logo': {'size': 100, 'type': 'image/png', 'extension': 'png'},
        'loading': {'size': 100, 'type': 'image/png', 'extension': 'png'},
        'undraw_dashboard': {'size': 200, 'type': 'image/svg+xml', 'extension': 'svg'},
    }

    def __init__(self, *args, **kwargs):
        images_url = {}
        images_path = {}
        images_is_clearable = {}
        for field_name in self.image_fields:
            images_url[field_name] = kwargs.pop(field_name + '_image_url')
            with open(kwargs.pop(field_name + '_image_path'), 'r') as file:
                images_path[field_name] = File(file)
            images_is_clearable[field_name] = kwargs.pop(field_name + '_image_is_clearable')

        super().__init__(*args, **kwargs)

        for field_name in self.color_fields:
            self.fields[field_name] = forms.CharField(label=self.labels[field_name], widget=ColorWidget)
            self.fields[field_name].icon = "palette"

        for field_name in self.image_fields:
            self.fields[field_name] = SVGAndImageFormField(
                required=False,
                validators=[FileExtensionValidator(allowed_extensions=[self.images_parameters[field_name]['extension']])],
                widget=TetrasBIImageInputWidget(
                    delete_label=_("Delete image"),
                    delete_icon='image-off-outline',
                    delete_on_icon="<i class='mdi mdi-check mr-1'></i>",
                    delete_off_icon="<i class='mdi mdi-close mr-1'></i>",
                    delete_on_text=_("Yes"),
                    delete_off_text=_("No"),
                    delete_on_style='danger',
                    delete_off_style='secondary',
                    input_label=self.labels[field_name],
                    input_icon='image-outline',
                    input_accept=self.images_parameters[field_name]['type'],
                    input_browse_text=_("Choose file"),
                    is_clearable=images_is_clearable[field_name],
                    image_size=self.images_parameters[field_name]['size'],
                    wrapper_classes='mw-50 ml-3 '
                )
            )
            self.fields[field_name].initial = images_path[field_name]
            self.fields[field_name].initial.url = images_url[field_name]
