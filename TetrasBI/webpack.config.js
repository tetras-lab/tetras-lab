const webpack = require("webpack");
const path = require('path');
const Dotenv = require('dotenv-webpack');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

module.exports = (env) => {
    console.log('ENV: ', process.env.ENV); // 'local'
    console.log('undrawNewColor: ', process.env.undrawNewColor);
    console.log('undrawNewColorRgb', process.env.undrawNewColorRgb);   
  
    return {
        mode: "development",
        entry: {
            main: "./TetrasBI/templates/static/entrypoints/main.js",
            d3components: "./TetrasBI/templates/static/entrypoints/d3components.js"
        },
        devtool: "inline-source-map",
        plugins: [
            new Dotenv({
                // path: `./.env.${env}`
                path: '../.env', // Path to .env file ('./.env' is default)
                allowEmptyValues: true, // allow empty variables (e.g. `FOO=`) (treat it as empty string, rather than missing)
                systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
                silent: false, // hide any errors
                defaults: false // load '.env.defaults' as the default values if empty.
            }),
            new CleanWebpackPlugin(),
            new WebpackManifestPlugin(),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                'window.jQuery': 'jquery',
            }),
        ],
        output: {
            // filename: "js/webpacked.js",
            filename: 'js/[name].bundle.js',
            path: path.join(__dirname, "build"),
            // 	path: path.resolve(__dirname, 'dist'),
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.(scss)$/,
                    use: [
                        {
                            // Adds CSS to the DOM by injecting a `<style>` tag
                            loader: "style-loader",
                        },
                        {
                            // Interprets `@import` and `url()` like `import/require()` and will resolve them
                            loader: "css-loader",
                        },
                        {
                            // Loader for webpack to process CSS with PostCSS
                            loader: "postcss-loader",
                            options: {
                                postcssOptions: {
                                    plugins: [
                                        // [
                                        // 	"postcss-preset-env",
                                        // 	{
                                        // 	// Options
                                        // 	},
                                        // ],
                                        [
                                            "autoprefixer",
                                            {
                                            // Options
                                            },
                                        ],
                                    ],
                                },
                            },
                        },
                        {
                            // Loads a SASS/SCSS file and compiles it to CSS
                            loader: "sass-loader",
                            options: {
                                additionalData:
                                    ""
                                    + "$undrawNewColor: #" + process.env.undrawNewColor + ";"
                                    + "$undrawNewColorRgb: " + process.env.undrawNewColorRgb + ";"
                                    + "@import '../theme/default/theme.scss';"
                                    + "@import '../theme/custom/theme.scss';"
                                    ,
                            }
                        },
                    ],
                },
                {
                    test: /\.m?js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    // Exposes jQuery for use outside Webpack build
                    test: require.resolve('jquery'),
                    loader: "expose-loader",
                    options: {
                        // exposes: ["$", "jQuery"],
                        exposes: [
                            {
                                globalName: "$",
                                override: true,
                            },
                            {
                                globalName: "jQuery",
                                override: true,
                            },
                        ],
                    },
                }
            ],
        },
    };
};
