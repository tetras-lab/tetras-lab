import MySQLdb
from os import environ
db = MySQLdb.connect(user="root", passwd=environ.get("DB_PASSWORD"), host=environ.get('DB_HOST'))
cur = db.cursor()
# cur.execute("create database test_tetrasbi;")
cur.execute("grant all privileges on test_tetrasbi.* to tetrasbi;")
cur.close()
db.commit()
