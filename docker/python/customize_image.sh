#!/bin/bash

install_geo() {
apt-get update
apt-get -y install libgdal-dev gdal-bin postgis
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal
pip install GDAL==3.2.2 psycopg2
pip install geopandas>=0.10.2
apt-get install -y libspatialindex-dev
pip install rtree>=1.0.0
}

install_java() {
apt-get -y install maven openjdk-11-jdk
}

while [ ! -z "$1" ]; do
    case $1 in
        "geo")
            install_geo
            ;;
        "java")
            install_java
            ;;
        *)
            echo "Unknown flavor $1"
            ;;
    esac
    shift
done

