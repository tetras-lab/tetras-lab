#!/bin/bash
HOST="0.0.0.0"
PORT="3031"
run_prod_server() {
    while true; do
        uvicorn --workers $NUM_THREADS --timeout-graceful-shutdown $TIMEOUT --port $PORT --host $HOST TetrasBI.asgi:application
    done
}
if [ "$ENV" != "prod" ]; then
    python manage.py runserver $HOST:$PORT &
    PID=$!
else
    run_prod_server &
    PID=$!
fi
/update.sh
python manage.py jupyterlab_dashboard_acl &

mkdir -p /opt/dashboards
mkdir -p /opt/dashboards/static
mkdir -p /opt/data
/flavor_entrypoint.sh
/save_boot_state.sh django

if [ "$ENV" == "prod" ]; then
    kill $(ps aux | awk '/uvicorn/{print $2}')
else
    python manage.py graph_models -a -o models.png
fi
wait $PID
