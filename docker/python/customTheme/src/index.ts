import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { IThemeManager } from '@jupyterlab/apputils';

import { ISettingRegistry } from '@jupyterlab/settingregistry';

/**
 * Initialization data for the customTheme extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'customTheme:plugin',
  autoStart: true,
  requires: [IThemeManager],
  optional: [ISettingRegistry],
  activate: async (app: JupyterFrontEnd, manager: IThemeManager, settingRegistry: ISettingRegistry | null) => {
    console.log('JupyterLab extension customTheme v3 is activated!');
    const style = 'customTheme/index.css';

    manager.register({
      name: 'customTheme',
      isLight: true,
      load: () => manager.loadCSS(style),
      unload: () => Promise.resolve(undefined)
    });

    if (settingRegistry) {
      settingRegistry
        .load(plugin.id)
        .then(settings => {
          console.log('customTheme settings loaded:', settings.composite);
        })
        .catch(reason => {
          console.error('Failed to load settings for customTheme.', reason);
        });
    }

    app.restored.then(() => {
      if (manager.theme !== 'customTheme') {
          manager.setTheme('customTheme');
      }
      const defaultColor = "transparent";
      const color = decodeURIComponent(new URLSearchParams(window.location.search).get('theme') || "") || defaultColor;
      (document.querySelectorAll('#jp-main-dock-panel')[0] as HTMLElement).style.backgroundColor = color;

      /* In case a user wants to reinitialize the theme */
      const callbackThemeChanged = (): void => {
        if (manager.theme !== 'customTheme') {
          (document.querySelectorAll('#jp-main-dock-panel')[0] as HTMLElement).style.backgroundColor = defaultColor;
        } else {
          (document.querySelectorAll('#jp-main-dock-panel')[0] as HTMLElement).style.backgroundColor = color;
        }
      };
      manager.themeChanged.connect(callbackThemeChanged);
    });

  }
};

export default plugin;
