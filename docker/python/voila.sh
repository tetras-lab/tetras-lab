#!/bin/bash

pip install -Ur requirements.txt
pip install -e /tetraslib/tetraslab
/flavor_entrypoint.sh
/save_boot_state.sh voila
if [ "$ENV" != "prod" ]; then
    debug="--debug"
fi
voila --no-browser $debug \
    --enable_nbextensions=True \
    --strip_sources=False \
    --MappingKernelManager.cull_interval=3600 --MappingKernelManager.cull_idle_timeout=21600 \
    --port $VOILA_PORT  --server_url=$VOILA_PATH --base_url=$VOILA_PATH \
    --Voila.tornado_settings="{'allow_origin': '$PROXY_HOST'}" \
    --Voila.ip=voila \
    $DASHBOARD_PATH
