#!/bin/bash
pip install -Ur /opt/requirements.txt
pip install -e /tetraslib/tetraslab
pip install -e /tetraslib/tl-jupyter-identity
/flavor_entrypoint.sh
#if [ "$ENV" != "prod" ]; then
#    #debug="--debug"
#fi

su jupyter -c "jupyter server --generate-config -y"

JUPYTER_KILL_KERNEL_TIME=${JUPYTER_KILL_KERNEL_TIME:-7200}

cat <<EOF >> /home/jupyter/.jupyter/jupyter_server_config.py
import tl_jupyter_identity
c.ServerApp.identity_provider_class = tl_jupyter_identity.TlJupyterAuthenticator
c.ServerApp.authorizer_class = tl_jupyter_identity.TlJupyterAuthorizer
c.MappingKernelManager.cull_idle_timeout = $JUPYTER_KILL_KERNEL_TIME
c.MappingKernelManager.cull_interval = $(( $JUPYTER_KILL_KERNEL_TIME / 4 ))
c.MappingKernelManager.cull_connected = True
c.MappingKernelManager.cull_busy = False
EOF

chown -R jupyter:jupyter /opt
cd /home/jupyter
su jupyter -c "jupyter lab --NotebookApp.base_url=$JUPYTER_PATH /opt $debug --ip 0.0.0.0 --port $JUPYTER_PORT --no-browser --SchedulerApp.db_url=sqlite:////opt/data/scheduler.sqlite --ServerApp.allow_origin=$PROXY_HOST"
