#!/bin/bash

# Avoid git errors on startup
git config --global --add safe.directory /app
npm install
python manage.py migrate
# Compile translations
python manage.py compilemessages
# Generate assets
python manage.py collectstatic --no-input
webpack="npx webpack --config webpack.config.js"
$webpack
python manage.py collectstatic --no-input
if [ "$ENV" == "dev" ]; then
    $webpack --watch &
fi
